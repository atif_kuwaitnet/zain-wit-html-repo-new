<?php $ishome = 1; include_once "includes/header.php";?>
	<div class="mediaGallery">
		<section id="introduction" data-color-theme="light">
			<div class="mainBanner" role="region" aria-label="mainBanner"> 
				<div class="banner__slide" role="group">
					<div class="banner__slide--image">
						<img
							src="src/images/banners/new_pressRelease__banner.png"
							alt="main banner image"
						/>
					</div>
					<div class="banner__slide--caption">
						<div class="container">
							<h1>media gallery</h1>
							<p>
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s.
							</p>
						</div>
					</div>
				</div>
				<a
					href="#"
					class="scrollDown"
					role="button"
					aria-label="Scroll to Contents"
				>
				<span></span>
				</a>
			</div>
		</section>
		<section class="contentWrapper" id="pressReleasesec" role="region" aria-label="press release section">
			<div class="container">
				<div class="tabs">
					<ul class="nav nav-pills" id="pills-tab" role="tablist">
						<li class="nav-item" role="presentation">
							<a class="nav-link active" data-toggle="pill" href="#news" role="tab" aria-controls="pills-news" aria-selected="true">News</a>
						</li>
						<li class="nav-item" role="presentation">
							<a class="nav-link" data-toggle="pill" href="#event" role="tab" aria-controls="pills-event" aria-selected="false">Events</a>
						</li>
						<li class="nav-item" role="presentation">
							<a class="nav-link" data-toggle="pill" href="#gallery" role="tab" aria-controls="pills-gallery" aria-selected="false">gallery</a>
						</li>
					</ul>
					<div class="tab-content mt-20">
						<div class="tab-pane fade show active" id="news" role="tabpanel" aria-labelledby="pills-news-tab">
							<div class="pressReleaseCard" aria-label="PRESS RELEASE" role="region">
								<div class="row">
									<div class="col-lg-4 col-sm-6 card_parent">
										<div class="card">
											<div class="image">
												<img
													src="src/images/press-release/towerXchange.png" 
													alt="Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire preciseZain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise"
													class="img-fluid"
												/>
											</div>
											<div class="date" id="0">26 Jan 2021</div>  
											<h4>
												Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire preciseZain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise
											</h4>
											<a href="gallery-detail.php" aria-describedby="0"><span class="btn btn-secondary">Explore more</span> </a>
										</div>
									</div>
									<div class="col-lg-4 col-sm-6 card_parent">
										<div class="card">
											<div class="image">
												<img
													src="src/images/press-release/towerXchange.png" 
													alt="Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise"
													class="img-fluid"
												/>
											</div>
											<div class="date" id="1">26 Jan 2021</div>  
											<h4>
											Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise
											</h4>
											<a href="gallery-detail.php" aria-describedby="0"><span class="btn btn-secondary">Explore more</span> </a>
										</div>
									</div>
									<div class="col-lg-4 col-sm-6 card_parent">
										<div class="card">
											<div class="image">
												<img
													src="src/images/press-release/towerXchange.png" 
													alt="Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise"
													class="img-fluid"
												/>
											</div>
											<div class="date" id="2">26 Jan 2021</div>  
											<h4>
											Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise
											</h4>
											<a href="gallery-detail.php" aria-describedby="0"><span class="btn btn-secondary">Explore more</span> </a>
										</div>
									</div>
									<div class="col-lg-4 col-sm-6 card_parent">
										<div class="card">
											<div class="image">
												<img
													src="src/images/press-release/towerXchange.png" 
													alt="Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire preciseZain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise"
													class="img-fluid"
												/>
											</div>
											<div class="date" id="0">26 Jan 2021</div>  
											<h4>
												Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire preciseZain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise
											</h4>
											<a href="gallery-detail.php" aria-describedby="0"><span class="btn btn-secondary">Explore more</span> </a>
										</div>
									</div>
									<div class="col-lg-4 col-sm-6 card_parent">
										<div class="card">
											<div class="image">
												<img
													src="src/images/press-release/towerXchange.png" 
													alt="Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise"
													class="img-fluid"
												/>
											</div>
											<div class="date" id="1">26 Jan 2021</div>  
											<h4>
											Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise
											</h4>
											<a href="gallery-detail.php" aria-describedby="0"><span class="btn btn-secondary">Explore more</span> </a>
										</div>
									</div>
									<div class="col-lg-4 col-sm-6 card_parent">
										<div class="card">
											<div class="image">
												<img
													src="src/images/press-release/towerXchange.png" 
													alt="Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise"
													class="img-fluid"
												/>
											</div>
											<div class="date" id="2">26 Jan 2021</div>  
											<h4>
											Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise
											</h4>
											<a href="gallery-detail.php" aria-describedby="0"><span class="btn btn-secondary">Explore more</span> </a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="event" role="tabpanel" aria-labelledby="pills-event-tab">
							<div class="pressReleaseCard" aria-label="PRESS RELEASE" role="region">
									<div class="row">
										<div class="col-lg-4 col-sm-6 card_parent">
											<div class="card">
												<div class="image">
													<img
														src="src/images/press-release/towerXchange.png" 
														alt="Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire preciseZain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise"
														class="img-fluid"
													/>
												</div>
												<div class="date" id="0">26 Jan 2021</div>  
												<h4>
													Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire preciseZain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise
												</h4>
												<a href="gallery-detail.php" aria-describedby="0"><span class="btn btn-secondary">Explore more</span> </a>
											</div>
										</div>
										<div class="col-lg-4 col-sm-6 card_parent">
											<div class="card">
												<div class="image">
													<img
														src="src/images/press-release/towerXchange.png" 
														alt="Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise"
														class="img-fluid"
													/>
												</div>
												<div class="date" id="1">26 Jan 2021</div>  
												<h4>
												Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise
												</h4>
												<a href="gallery-detail.php" aria-describedby="0"><span class="btn btn-secondary">Explore more</span> </a>
											</div>
										</div>
										<div class="col-lg-4 col-sm-6 card_parent">
											<div class="card">
												<div class="image">
													<img
														src="src/images/press-release/towerXchange.png" 
														alt="Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise"
														class="img-fluid"
													/>
												</div>
												<div class="date" id="2">26 Jan 2021</div>  
												<h4>
												Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise
												</h4>
												<a href="gallery-detail.php" aria-describedby="0"><span class="btn btn-secondary">Explore more</span> </a>
											</div>
										</div>
									</div>
								</div>
							</div>
						<div class="tab-pane fade" id="gallery" role="tabpanel" aria-labelledby="pills-gallery-tab">
							<div class="pressReleaseCard" aria-label="PRESS RELEASE" role="region">
								<div class="row">
									<div class="col-lg-4 col-sm-6 card_parent">
										<div class="card">
											<div class="image">
												<img
													src="src/images/press-release/towerXchange.png" 
													alt="Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire preciseZain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise"
													class="img-fluid"
												/>
											</div>
											<div class="date" id="0">26 Jan 2021</div>  
											<h4>
												Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire preciseZain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise
											</h4>
											<a href="gallery-detail.php" aria-describedby="0"><span class="btn btn-secondary">Explore more</span> </a>
										</div>
									</div>
									<div class="col-lg-4 col-sm-6 card_parent">
										<div class="card">
											<div class="image">
												<img
													src="src/images/press-release/towerXchange.png" 
													alt="Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise"
													class="img-fluid"
												/>
											</div>
											<div class="date" id="1">26 Jan 2021</div>  
											<h4>
											Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise
											</h4>
											<a href="gallery-detail.php" aria-describedby="0"><span class="btn btn-secondary">Explore more</span> </a>
										</div>
									</div>
									<div class="col-lg-4 col-sm-6 card_parent">
										<div class="card">
											<div class="image">
												<img
													src="src/images/press-release/towerXchange.png" 
													alt="Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise"
													class="img-fluid"
												/>
											</div>
											<div class="date" id="2">26 Jan 2021</div>  
											<h4>
											Zain Drone assigned by Kuwait’s Public Authority of Industry to acquire precise
											</h4>
											<a href="gallery-detail.php" aria-describedby="0"><span class="btn btn-secondary">Explore more</span> </a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="pressReleasePagination" aria-label="PRESS RELEASE PAGINATION" role="region">
					<div class="show_items">
						<label class="items__show">show items</label> 
						<div class="form-group">
							<select class="selectpicker" title="100">
								<option>100</option>
								<option>200</option>
								<option>300</option>
								<option>400</option>
							</select>
						</div>
					</div>
					<div class="overflow-auto">
						<nav aria-label="Page navigation example">
							<ul class="pagination justify-content-center">
								<li class="page-item">
									<a class="page-link" href="#" aria-label="Previous">
									</a>
								</li>
								<li class="page-item active"><a class="page-link" href="#">1</a></li>
								<li class="page-item"><a class="page-link" href="#">2</a></li>
								<li class="page-item"><a class="page-link" href="#">3</a></li>
								<li class="page-item"><a class="page-link" href="#">4</a></li>
								<li class="page-item">
									<a class="page-link" href="#" aria-label="Next">
									</a>
								</li>
							</ul>
						</nav>
					</div>
					<div class="gotoPage">
					<label class="items__show">go to page</label>
					<div class="form-group">
						<input
							type="text"
							class="form-control"
							
							id="goBtn"
						/>
						<button class="btn btn-primary" aria-describedby="goBtn">
							Go
						</button>
					</div>
				</div>
				</div>
			</div>
		</section>
	</div>
<?php include_once "includes/footer.php";?>