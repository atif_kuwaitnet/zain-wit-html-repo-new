<?php $ishome = 1; include_once "includes/header.php";?>
    <div class="contactUs">
		<section id="introduction" data-color-theme="light">
			<div class="mainBanner" role="region" aria-label="introduction">
				<div class=" banner__slide" role="group">
					<div class="banner__slide--image">
						<img
							src="src/images/banners/new_contactUs__banner.png"
							alt="contact us banner"
						/>
					</div>
					<div class="banner__slide--caption">
						<div class="container">
							<h1>Contact US</h1>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s.</p>
						</div>
					</div>
				</div>
				<a
					href="#"
					class="scrollDown"
					role="button"
					aria-label="Scroll to Contents"
				>
				<span></span>
				</a>
			</div>	 
		</section>
		<div class="contentWrapper" role="region" aria-label="Contact Us" id="contactUs" data-color-theme="dark">
            <div class="container">
				<div class="contactUs__form" > 
					<form role="contact Form"> 
						<fieldset>
							<legend class="visuallyhidden">&nbsp;</legend>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Full name<span>*</span></label>
										<input type="text" class="form-control" aria-label="" placeholder="Full name.."/>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Email<span>*</span></label>
										<input type="email" class="form-control" aria-label="" placeholder="Email.."/>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Phone Number<span>*</span></label>
										<input type="tell" class="form-control number" aria-label="" placeholder="Contact Number.."/>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Country of Residence<span>*</span></label>
										<select class="selectpicker" title="Country of Residence">
											<option>All</option>
											<option>This is First option</option>
											<option>Selected Option</option>
											<option>Third Option</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Nationality<span>*</span></label>
										<select class="selectpicker" title="Nationality">
											<option>All</option>
											<option>This is First option</option>
											<option>Selected Option</option>
											<option>Third Option</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Address<span>*</span></label>
										<input type="text" class="form-control" aria-label="" placeholder="Address"/>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">University<span>*</span></label>
										<input type="text" class="form-control" aria-label="" placeholder="University"/>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Major<span>*</span></label>
										<input type="text" class="form-control" aria-label="" placeholder="Major"/>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Expected year of graduation<span>*</span></label>
										<select class="selectpicker" title="Expected year of graduation">
											<option>All</option>
											<option>This is First option</option>
											<option>Selected Option</option>
											<option>Third Option</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Current GPA<span>*</span></label>
										<input type="text" class="form-control" aria-label="" placeholder="Current GPA"/>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label">Please list careers, fields, or industries that you are interested in learning more about<span>*</span></label>
										<input type="text" class="form-control" aria-label="" placeholder="Please list careers, fields, or industries that you are interested in learning more about"/>
									</div>
								</div> 
							</div>
							<div class="row">
								<div class="col-md-8">
									<div class="checkboxWrapper">
										<div class="form-group">
											<label class="control-label">Are you interested in help with (please select as many as applicable)</label>
											<ul>
												<li>
													<div class="custom-control custom-control-inline custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="checkbox-1">
														<label class="custom-control-label" for="checkbox-1">Building your network</label>
													</div>
												</li>
												<li>
													<div class="custom-control custom-control-inline custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="checkbox-2">
														<label class="custom-control-label" for="checkbox-2">Setting career goals</label>
													</div>
												</li>
												<li>
													<div class="custom-control custom-control-inline custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="checkbox-3">
														<label class="custom-control-label" for="checkbox-3">Creating career opportunities</label>
													</div>
												</li>
												<li>
													<div class="custom-control custom-control-inline custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="checkbox-4">
														<label class="custom-control-label" for="checkbox-4">Personal development</label>
													</div>
												</li>
												<li>
													<div class="custom-control custom-control-inline custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="checkbox-5">
														<label class="custom-control-label" for="checkbox-5">Setting life goals</label>
													</div>
												</li>
												<li>
													<div class="custom-control custom-control-inline custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="checkbox-6">
														<label class="custom-control-label" for="checkbox-6">Balancing the demands of life</label>
													</div>
												</li>
												<li>
													<div class="custom-control custom-control-inline custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="checkbox-7">
														<label class="custom-control-label" for="checkbox-7">Expanding your knowledge of STEM career paths</label>
													</div>
												</li>
												<li>
													<div class="custom-control custom-control-inline custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="checkbox-8">
														<label class="custom-control-label" for="checkbox-8">Creating education goals/plan</label>
													</div>
												</li>
												<li>
													<div class="custom-control custom-control-inline custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="checkbox-9">
														<label class="custom-control-label" for="checkbox-9">Creating career goals/plan</label>
													</div>
												</li>
												<li>
													<div class="custom-control custom-control-inline custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="checkbox-10">
														<label class="custom-control-label" for="checkbox-10">Progressing in your current field</label>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">Attach and include C.V.</label>
										<div class="fileUpload">
											<div class="custom-file">
												<input type="file" class="custom-file-input">
												<label class="custom-file-label">
													<span>No file chosen</span>
												</label>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label pl-0 pr-0">Note:</label>
										<p>All prospective mentees must be women who are currently be studying and enrolled in university majoring in a STEM-related degree.</p>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="g-recaptcha brochure__form__captcha" data-sitekey="YOUR SITE KEY"></div>
							</div>
							<div class="form-group">
								<div class="button-row">
									<button type="submit" class="btn btn-primary" aria-describedby="contactform">
										submit
									</button>
								</div>
							</div>
						</fieldset>
					</form>
				</div> 
            </div>
        </div>		
    </div>
<?php include_once "includes/footer.php";?>