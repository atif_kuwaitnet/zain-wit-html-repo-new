<?php $ishome = 1; include_once "includes/header.php";?>
    <div class="mediaGallery">
		<section id="introduction" data-color-theme="light">
			<div class="mainBanner" role="region" aria-label="introduction">
				<div class=" banner__slide" role="group">
					<div class="banner__slide--image">
						<img
							src="src/images/banners/new_pressRelease__banner.png"
							alt="press release banner"
						/>
					</div>
					<div class="banner__slide--caption">
						<div class="container">
							<h1>الدورات</h1>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s.</p>
						</div>
					</div>
				</div>
				<a
					href="#"
					class="scrollDown"
					role="button"
					aria-label="Scroll to Contents"
				>
				<span></span>
				</a>
			</div>	 
		</section>	
		<div class="contentWrapper">
			<div class="container">
				<section id="courseList" role="region" aria-label="Course List">
					<div class="courseList">
						<ul class="courseList__box">
							<li class="courseList__box--item">
								<span class="image">
									<img src="src/images/icons/courseList/video.png" alt="" class="img-fluid">
								</span>
								<div class="desc">
									<h2>45+ فصول</h2>
									<span>من السادة</span>
								</div>
							</li>
							<li class="courseList__box--item">
								<span class="image">
									<img src="src/images/icons/courseList/playlist.png" alt="" class="img-fluid">
								</span>
								<div class="desc">
									<h2>24 lessons</h2>
									<span>average per class</span>
								</div>
							</li>
							<li class="courseList__box--item">
								<span class="image">
									<img src="src/images/icons/courseList/clock.png" alt="" class="img-fluid">
								</span>
								<div class="desc">
									<h2>12 minutes</h2>
									<span>average per lesson</span>
								</div>
							</li>
						</ul>
					</div>
				</section>
				<section id="courseBox" role="region" aria-label="Course Box">
					<div class="course row justify-content-center">
						<div class="col-lg-3 col-md-4 col-sm-6 course__card">
							<div class="course__card--image">
								<img src="src/images/courses.png" alt="" class="img-fluid">
							</div>
							<div class="course__card--content">
								<h2>عنوان الدورة</h2>
								<div class="country">
									<span class="country--icon">
										<img src="src/images/country/kuwait.png" alt="" class="img-fluid">
									</span>
									<span class="country--name">الكويت</span>
								</div>
								<p>الألم بحد ذاته هو الكثير من الألم ، لقد كان حزينًا على مر السنين ، لكن حان الوقت لحسد الألم والألم مثير ، كان مثيرًا ، لكنه كان معقدًا. لكنهم دعموا أيضًا كلاً من المحطة والآلام الوحيدة. دع كليتا</p>
								<span class="unit">128 دينار كويتي</span>
								<div class="links">
									<ul class="linksStyle">
										<li>
											<a href="course-detail.php" class="btn">عرض التفاصيل</a>
										</li>
										<li>
											<a href="#" class="btn">يتسجل، يلتحق</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 course__card">
							<div class="course__card--image">
								<img src="src/images/courses.png" alt="" class="img-fluid">
							</div>
							<div class="course__card--content">
								<h2>course title</h2>
								<div class="country">
									<span class="country--icon">
										<img src="src/images/country/kuwait.png" alt="" class="img-fluid">
									</span>
									<span class="country--name">Kuwait</span>
								</div>
								<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita</p>
								<span class="unit">128 KWD</span>
								<div class="links">
									<ul class="linksStyle">
										<li>
											<a href="course-detail.php" class="btn">View Details</a>
										</li>
										<li>
											<a href="#" class="btn">Enroll</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 course__card">
							<div class="course__card--image">
								<img src="src/images/courses.png" alt="" class="img-fluid">
							</div>
							<div class="course__card--content">
								<h2>course title</h2>
								<div class="country">
									<span class="country--icon">
										<img src="src/images/country/kuwait.png" alt="" class="img-fluid">
									</span>
									<span class="country--name">Kuwait</span>
								</div>
								<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita</p>
								<span class="unit">128 KWD</span>
								<div class="links">
									<ul class="linksStyle">
										<li>
											<a href="course-detail.php" class="btn">View Details</a>
										</li>
										<li>
											<a href="#" class="btn">Enroll</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 course__card">
							<div class="course__card--image">
								<img src="src/images/courses.png" alt="" class="img-fluid">
							</div>
							<div class="course__card--content">
								<h2>course title</h2>
								<div class="country">
									<span class="country--icon">
										<img src="src/images/country/kuwait.png" alt="" class="img-fluid">
									</span>
									<span class="country--name">Kuwait</span>
								</div>
								<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita</p>
								<span class="unit">128 KWD</span>
								<div class="links">
									<ul class="linksStyle">
										<li>
											<a href="course-detail.php" class="btn">View Details</a>
										</li>
										<li>
											<a href="#" class="btn">Enroll</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 course__card">
							<div class="course__card--image">
								<img src="src/images/courses.png" alt="" class="img-fluid">
							</div>
							<div class="course__card--content">
								<h2>course title</h2>
								<div class="country">
									<span class="country--icon">
										<img src="src/images/country/kuwait.png" alt="" class="img-fluid">
									</span>
									<span class="country--name">Kuwait</span>
								</div>
								<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita</p>
								<span class="unit">128 KWD</span>
								<div class="links">
									<ul class="linksStyle">
										<li>
											<a href="course-detail.php" class="btn">View Details</a>
										</li>
										<li>
											<a href="#" class="btn">Enroll</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 course__card">
							<div class="course__card--image">
								<img src="src/images/courses.png" alt="" class="img-fluid">
							</div>
							<div class="course__card--content">
								<h2>course title</h2>
								<div class="country">
									<span class="country--icon">
										<img src="src/images/country/kuwait.png" alt="" class="img-fluid">
									</span>
									<span class="country--name">Kuwait</span>
								</div>
								<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita</p>
								<span class="unit">128 KWD</span>
								<div class="links">
									<ul class="linksStyle">
										<li>
											<a href="course-detail.php" class="btn">View Details</a>
										</li>
										<li>
											<a href="#" class="btn">Enroll</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 course__card">
							<div class="course__card--image">
								<img src="src/images/courses.png" alt="" class="img-fluid">
							</div>
							<div class="course__card--content">
								<h2>course title</h2>
								<div class="country">
									<span class="country--icon">
										<img src="src/images/country/kuwait.png" alt="" class="img-fluid">
									</span>
									<span class="country--name">Kuwait</span>
								</div>
								<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita</p>
								<span class="unit">128 KWD</span>
								<div class="links">
									<ul class="linksStyle">
										<li>
											<a href="course-detail.php" class="btn">View Details</a>
										</li>
										<li>
											<a href="#" class="btn">Enroll</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 course__card">
							<div class="course__card--image">
								<img src="src/images/courses.png" alt="" class="img-fluid">
							</div>
							<div class="course__card--content">
								<h2>course title</h2>
								<div class="country">
									<span class="country--icon">
										<img src="src/images/country/kuwait.png" alt="" class="img-fluid">
									</span>
									<span class="country--name">Kuwait</span>
								</div>
								<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita</p>
								<span class="unit">128 KWD</span>
								<div class="links">
									<ul class="linksStyle">
										<li>
											<a href="course-detail.php" class="btn">View Details</a>
										</li>
										<li>
											<a href="#" class="btn">Enroll</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
                </section>
			</div>
		</div>	
    </div>
<?php include_once "includes/footer.php";?>