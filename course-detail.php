<?php $ishome = 1; include_once "includes/header.php";?>
    <div class="courseDetail">
		<section id="header" data-color-theme="light">
			<div class="mainBanner" role="region" aria-label="mainBanner">
				<div class=" banner__slide" role="group">
					<div class="banner__slide--image">
						<img
							src="src/images/banners/innerBanner-1920x1000.png"
							alt="main banner image"
						/>
					</div>
					<div class="banner__slide--caption">
						<div class="container">
							<h1>Your Children are<br>safe at home<br> are they safe online?</h1>
							<p>More than 30% of all internet users are children and with the expansion of the broadband it will radically increase</p>
						</div>
					</div>
				</div>
				<a
					href="#"
					class="scrollDown"
					role="button"
					aria-label="Scroll to Contents"
				>
				<span></span>
				</a>
			</div>	 
		</section>	
		<section data-color-theme="light" id="detail">
			<div class="courseDetal" aria-label="course Detail">
				<div class="container">
					<h2 class="sectionTitle">Course Name Goes Here</h2>
					<div class="row">
						<div class="col-lg-8 col-md-7">
							<div class="main_content">
								<h3 class="title">
									<div>
										<svg viewBox="0 0 45 45" fill="currentColor" >
											<g id="shiningStar-icon">
												<image id="Vector_Smart_Object" data-name="Vector Smart Object" width="45" height="45" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAAtCAYAAAA6GuKaAAAFH0lEQVRYhdWZXYhVVRTHf44K6mhmWBYU48OY0xhWdpm0CIvogyLtwx6ymigffJiYWw8+2JVI5EQQBCeohwIHAycl+rBsoIFsKhizjiTWaE061VSI9GSSiZMZy/nfZs/Z59x77tx7k/5w2B9r7bX/d5+z91p73Umd1z5NjXE18AKwCBgA1gH70qYIo8DrK4cpZeSGxcAtwKvAH550POYBHwEXqPcStVuBo562g3yuMANYA3wcRsE3noKDBq/HRzfwIrDFk/hY7xDeqtLa6z1NH68DLwFveJIJkP5S5f1aiTRcDKyVbAfwsErDWskTkc8V1si+O18qspB+EvhB9Uc96Xi9aapvipXTJE/D4+q3eZ5K0fkXWUgfA+4B3gKe86SjmAs8ofqHwF7V96qN5HO9kaOw3fgucG8YBcc8aQxZNqJhP7DK6x2DrU6jWs/GZNa+XXLTK8QHh1HQA/R4VlOQZaXL4XygQzq7gM9j+tbepXqH9KtCLUjbtzpb9U2edHz/7DLfdiZUS3om0Kn6J0CfpzGKPsmR/kxP4z8knQfmqP68Jx2PonyOxk0YWdz4JOBSYAFwuZ4FehZKZw+w1Bvpw77v69Q7qOf7WP2XMArOeCNdQgmkbwNuEqFmkZvujRzD79L/ypP4uEafynmeZAx/ivwh4DvTD6OgtxTpzcBjnhmfpBn8Fjgg9/uzp5WOy4B2xSMtWphSP8LQFUZB0QGNI22vd7fqJ0Us/uoGywU+lcKivHyuMM/57Nyy2fGyy8IoOHucus7lKqduK/BTLcmVQhgFR7UYn7lq+VyhCfjR4XeWtHt6DDr1LmBGiXnqDoWqXUn8XNJ9ii8MNwM7zxVxEd4pHoa3XR/gkrZjZjXwvtrnhHgCYePzoHsMxp3LKQVGLvEex03Xm7DN80GM8KowCk65ekkeMU58OdBbb+Ii3KszP5VwGmkc4tvVbqsncYdwm7q2pxEmwbnEMVnOY7X6v5DHLBuoV4A4YbuTtodRcDrNRNpKF3Fa3qtbbTO8zdOqDtsqIZyFNA7xItk7gCZPa2Jokj1kvyzhrKSLxLc67ari4RQ7W7MQRm7c7m53Aw8AFwGPOK7TxRWq/w0c9qQTw2HZa5D9nXEr+VxhvvbVb8CbdqpM0Y15oaN3K/BaAoVFKocUUNUCJ2Wv2bEfh/G5UX33Wbhqv3CWOuyXvFxioy1WOeBJqsNAzH4cxucV8TPMatDOvV55N8tNHPeGjb6+FtX3e9LqULTXks8VvD0WRsHxMAo6xM94ttnn8aueUpjv3F4OlNCbCIr2pmueoSQb2qS7qeD0cF9dOdLLlC0aVGntUnDtpX0i45CVdHGT/AUc9KRjOpZw7AdW6vaxUu0dJTbaQdmlhM6ESLeqtNUbicmalAb+GlihvhOKzU+ovULyLXHHFEbBiBPgt5IBla60+yovtDl1d2xXqmFEO71ZAVez2iOSt0s/1Hhidq/0Zk5AVtLzVQ7piNyotKxli6bqAtGtlbKdfkT6R9RulfyM9Ds1fmM+V5jlbL5M4UFW0sMqO1R/xsmS2iVhCfCQbvBJOCT5Eic72ig7w04CczhhrIespDfI3TY6Wc9+XRDuKvVHUAz7pL9c45G9Rtnf4I1IQNb89HtypcVEzjuV5JMT8ClwA3CnYh4L9jeHUbAnQddDVtJoZfq93urQo4R6RahFfjoJSxUzZElKVoxKVroSrFNENtX516pmqNdKT46V/wvSdUW9SJsLt/RvMc1WOwD/AGxXRfiY+jewAAAAAElFTkSuQmCC"/>
											</g>
										</svg>
									</div>
									cource detail
								</h3>
								<div class="detail">
									<p>
											Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
											Ipsum has been the industry's standard dummy text ever since the 1500s, when an
											unknown printer took a galley of type and scrambled it to make a type specimen
											book. It has survived not only five centuries, but also the leap into electronic
											typesetting, remaining essentially unchanged. It was popularised in the 1960s
											with the release of Letraset sheets containing Lorem Ipsum passages, and more
											recently with desktop publishing software like Aldus PageMaker including
											versions of Lorem Ipsum.
									</p>
									<p>
											Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
											Ipsum has been the industry's standard dummy text ever since the 1500s, when an
											unknown printer took a galley of type and scrambled it to make a type specimen
											book. It has survived not only five centuries, but also the leap into electronic
											typesetting, remaining essentially unchanged. It was popularised in the 1960s
											with the release of Letraset sheets containing Lorem Ipsum passages, and more
											recently with desktop publishing software like Aldus PageMaker including
											versions of Lorem Ipsum.
									</p>
									<p>
											Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
											Ipsum has been the industry's standard dummy text ever since the 1500s, when an
											unknown printer took a galley of type and scrambled it to make a type specimen
											book. It has survived not only five centuries, but also the leap into electronic
											typesetting, remaining essentially unchanged. It was popularised in the 1960s
											with the release of Letraset sheets containing Lorem Ipsum passages, and more
											recently with desktop publishing software like Aldus PageMaker including
											versions of Lorem Ipsum.
									</p>
								</div>
								<h3 class="title">
									<div>
										<svg viewBox="0 0 38 38" fill="currentColor" >
										<g id="description-icon">
											<image id="Vector_Smart_Object" data-name="Vector Smart Object" width="38" height="38" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAACAUlEQVRYhe2YMUscQRTHfy7XyYE2NoqYQkUbC1ewEywEo1UabfwKLiJp3EIstrKQNSlsrGwUxCoBUfALuBBsFLVICKlFkBRKIvLgLciNzN556+we+INhuXcz+//ve7Nzx2tbHF1BqQJLwDzQD1Rwwz/gGtgFNuIkuhNVT6UHgTNgDRhyaArVGlLts8APxQuSsaqa+mAsKYafwIin5UtN3QOLQKeYdjQ6VfNePYiXpYruqZTPwBeXeYqT6FY0Az+Uj5sanvd0o6fsGCvd8Vy736vZ6LdFudLMpVQ8Y0ZJeDfWKKU1Zjvh+4DZjDn1IOfTHnCTl7GTHH8NZvQh68ZWynYj8nqqja60ZUyecC5jTj1IKbfyNHaqoxBspSwUW8aGgU8Zc5rhP/AN+PHSPWyiR0C3Ec2X5cAPu+Ikeqi9a0uWcspFKV/KVpaxcx2F0JKlHGvigJVDdRu4Mr7JwZi8yl1GtH4mgPEG1zymVbSV8q8RaYzXrD/Q63dbxiab+Nsjpdw3otnI1ukB/thEfwFfjegbEieRlPI3GaUslJY8LpwT+GEv8BE4LJUx4BgYkP3taX8qpcOY6ojADzvUlNDjadMsZaEoYzXa1xXt5K1pYF2vO676GJqphWfawm6pG3fS85zWQBlMTUsfNj3HLsUlsApc1LwQb41oiaZoj8RJdAnwBNM+ae9Pwk73AAAAAElFTkSuQmCC"/>
										</g>
										</svg>
									</div>
									Requirments
								</h3>
								<div class="detail">
									<p>
											Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
											Ipsum has been the industry's standard dummy text ever since the 1500s, when an
											unknown printer took a galley of type and scrambled it to make a type specimen
											book. It has survived not only five centuries, but also the leap into electronic
											typesetting, remaining essentially unchanged. It was popularised in the 1960s
											with the release of Letraset sheets containing Lorem Ipsum passages, and more
											recently with desktop publishing software like Aldus PageMaker including
											versions of Lorem Ipsum.
									</p>
									<p>
											Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
											Ipsum has been the industry's standard dummy text ever since the 1500s, when an
											unknown printer took a galley of type and scrambled it to make a type specimen
											book. It has survived not only five centuries, but also the leap into electronic
											typesetting, remaining essentially unchanged. It was popularised in the 1960s
											with the release of Letraset sheets containing Lorem Ipsum passages, and more
											recently with desktop publishing software like Aldus PageMaker including
											versions of Lorem Ipsum.
									</p>
									<p>
											Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
											Ipsum has been the industry's standard dummy text ever since the 1500s, when an
											unknown printer took a galley of type and scrambled it to make a type specimen
											book. It has survived not only five centuries, but also the leap into electronic
											typesetting, remaining essentially unchanged. It was popularised in the 1960s
											with the release of Letraset sheets containing Lorem Ipsum passages, and more
											recently with desktop publishing software like Aldus PageMaker including
											versions of Lorem Ipsum.
									</p>
								</div>

							</div>
						</div>
						<div class="col-lg-4 col-md-5">
							<div class="box ">
								<div class="imageSection">
									<img src="src/images/icons/char_17-01.jpg" class="img-fluid" />
								</div>
								<h5 class="namelabel">
									Johne doe
								</h5>
								<span class="subtitle">
									Lorem, ipsum, dolor.
								</span>
								<div class="detail">
									<p>Lorem ipsum dolor, sit amet consectetur, adipisicing elit. Tempora recusandae corrupti odit sapiente accusantium optio, quo sed perferendis voluptas architecto exercitationem deleniti ab perspiciatis accusamus animi aliquam saepe <a href="">. Praesentium, nisi?</a></p>
								</div>
							</div>
							<div class="box courseDetal__detail">
								<h5 class="boxTitle">Includes</h5>
								<ul class="courseDetal__detail--list">
									<li>
											Lorem ipsum dolor sit, amet consectetur.
									</li>
										<li>
											Lorem ipsum dolor sit, amet consectetur.
									</li>
										<li>
											Lorem ipsum dolor sit, amet consectetur.
									</li>
										<li>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt, unde.
									</li>
										<li>
											Lorem ipsum dolor sit, amet consectetur.
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>	
    </div>
<?php include_once "includes/footer.php";?>