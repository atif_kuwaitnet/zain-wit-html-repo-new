<?php $ishome = 1; include_once "includes/header.php";?>
	<div class="pressRelease">
		<section id="introduction" data-color-theme="light">
			<div class="mainBanner" role="region" aria-label="mainBanner"> 
				<div class="banner__slide" role="group">
					<div class="banner__slide--image">
						<img
							src="src/images/banners/new_pressRelease__banner.png"
							alt="main banner image"
						/>
					</div>
					<div class="banner__slide--caption">
						<div class="container">
							<h1>تفاصيل</h1>
							<p>
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s.
							</p>
						</div>
					</div>
				</div>
				<a
					href="#"
					class="scrollDown"
					role="button"
					aria-label="Scroll to Contents"
				>
				<span></span>
				</a>
			</div>
		</section>
		<div class="contentWrapper">
			<section data-color-theme="dark">
				<div class="container pressReleaseMain">
					<div class="titleWrapper">
						<div class="title">
							<h2>مجموعة زين تنشر تقريرًا ثاقبًا حول "المرأة في التكنولوجيا لسد الفجوة بين الجنسين في مجالات العلوم والتكنولوجيا والهندسة والرياضيات"</h2>
						</div>
						<a href="media-gallery.php" class="cross-icon">
							<span>
								<div>
									<svg viewBox="0 0 45 45" fill="currentColor">
									<g id="cross-icon2">
										<image id="Vector_Smart_Object" data-name="Vector Smart Object" width="45" height="45" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAAtCAYAAAA6GuKaAAABfUlEQVRYhe3Z0Q3CMAwEUC/BQkzFinyzC79BoFSC+u6aNKnTSJzkD8BxHiUCiVrOxczuZna14KSUNiu73r5LfvwBP97rzewZDS8EP7Pvkb2fd5C+KhSOoAS81J29EAZHWAH+cQ2D14KXMz0UXgteo+UC13kAWu2/6nOhC11nR7TaF3waMHQAam5Fq/3Imaehg9iCnaH7IPAWWl4B1xkELkHLwa4zAFyKlhu4zoPBNWi5kevUoXMQEFW3K+Q6ceh6hGO1J3TjjVl0HcOx2hsKIPNoP4OpagmF1BwphWPVGgkqeWMMpqpHGOxGnv/5JBSOVa8g+LoceDSaHQV6hc+CtnwkEPrmOk+Cnu5KK7CEI9T/2wOEgU/7PT3dL6KEgNB+hDoCTQGus3AdgqmKAneFR4K7waPBXeAjwM3wUeAmuAodKNaEwFnoINK/O6nTf3l0gOvshK6FDwV/o2vgQ8FrdCl8KBihS+Cywe0QhN6C26z3Eae8Y7vA57g3npK9AK9km9ut8md9AAAAAElFTkSuQmCC"/>
									</g>
									</svg>
								</div>
							</span>
						</a>
					</div>
					<!-- media slider -->
					<div class="mediaSlider">
						<div class="mediaSlider__slides">
							<div class="swiper">
								<div class="swiper-wrapper">
									<div class="swiper-slide">
										<div class="mediaSlider__item">
											<img src="src/images/news-detail/zainiacSlider-image.png" alt="" class="img-fluid">
										</div>
										<h2>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h2>
									</div>
									<div class="swiper-slide">
										<div class="mediaSlider__item">
											<img src="src/images/news-detail/zainiacSlider-image.png" alt="" class="img-fluid">
										</div>
										<h2>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h2>
									</div>
									<div class="swiper-slide">
										<div class="mediaSlider__item">
											<img src="src/images/news-detail/zainiacSlider-image.png" alt="" class="img-fluid">
										</div>
										<h2>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h2>
									</div>
								</div>
							</div>
							<div class="sliderNavigation">
								<div class="container">
									<div class="sliderNavigation__controls">
										<div class='sliderNavigation__controls--icon'>
											<svg class="slider-play" viewbox='0 0 163.861 163.861' fill='currentColor'>
												<g >
													<path
														d="M34.857,3.613C20.084-4.861,8.107,2.081,8.107,19.106v125.637c0,17.042,11.977,23.975,26.75,15.509L144.67,97.275
														c14.778-8.477,14.778-22.211,0-30.686L34.857,3.613z"
													/>
												</g>
											</svg>
											<svg class="slider-pause" viewbox='0 0 30.52 38.09' fill='currentColor'>
												<g>
													<path
														d="M10.61,33.53A5.31,5.31,0,0,1,0,33.53V6.05a5.31,5.31,0,1,1,10.61,0Zm0,0"
														transform="translate(0 -0.74)"
													/>
													<path
														d="M30.52,33.53a5.31,5.31,0,0,1-10.61,0V6.05a5.31,5.31,0,1,1,10.61,0Zm0,0"
														transform="translate(0 -0.74)"
													/>
												</g>
											</svg>
										</div>
										<div class="sliderNavigation__controls--pagination">
											<div class="swiper-pagination"></div>
										</div>
										<button type="button" role="button" class="sliderNavigation__controls--arrow prevArrow">
											<svg>
												<g id="left-arrow">
													<path
														fill-rule="evenodd"
														d="M0.809,12.572 L20.175,0.476 C20.483,0.287 20.851,0.400 21.066,0.759 C21.282,1.117 21.289,1.628 21.084,1.997 L14.715,13.488 L21.084,24.978 C21.289,25.347 21.282,25.860 21.067,26.216 C20.923,26.460 20.707,26.591 20.487,26.591 C20.382,26.591 20.276,26.561 20.176,26.498 L0.811,14.403 C0.546,14.237 0.377,13.881 0.377,13.488 C0.377,13.094 0.546,12.738 0.809,12.572 Z"
													/>
												</g>
											</svg>
										</button>
										<button type="button" role="button" class="sliderNavigation__controls--arrow nextArrow">
											<svg>
												<g id="right-arrow">
													<path
														fill-rule="evenodd"
														d="M21.190,12.572 L1.825,0.476 C1.517,0.287 1.149,0.400 0.934,0.759 C0.718,1.117 0.711,1.628 0.916,1.997 L7.285,13.488 L0.916,24.978 C0.711,25.347 0.718,25.860 0.933,26.216 C1.077,26.460 1.293,26.591 1.512,26.591 C1.618,26.591 1.724,26.561 1.824,26.498 L21.189,14.403 C21.454,14.237 21.623,13.881 21.623,13.488 C21.623,13.094 21.454,12.738 21.190,12.572 Z"
													/>
												</g>
											</svg>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- media slider -->
					<div class="highlight" id="highlight" aria-label="Highlights" role="region" data-color-theme="dark">
						<h3 class="title">
							<div>
								<svg viewBox="0 0 45 45" fill="currentColor">
									<g id="shiningStar-icon">
										<image id="Vector_Smart_Object" data-name="Vector Smart Object" width="45" height="45" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAAtCAYAAAA6GuKaAAAFH0lEQVRYhdWZXYhVVRTHf44K6mhmWBYU48OY0xhWdpm0CIvogyLtwx6ymigffJiYWw8+2JVI5EQQBCeohwIHAycl+rBsoIFsKhizjiTWaE061VSI9GSSiZMZy/nfZs/Z59x77tx7k/5w2B9r7bX/d5+z91p73Umd1z5NjXE18AKwCBgA1gH70qYIo8DrK4cpZeSGxcAtwKvAH550POYBHwEXqPcStVuBo562g3yuMANYA3wcRsE3noKDBq/HRzfwIrDFk/hY7xDeqtLa6z1NH68DLwFveJIJkP5S5f1aiTRcDKyVbAfwsErDWskTkc8V1si+O18qspB+EvhB9Uc96Xi9aapvipXTJE/D4+q3eZ5K0fkXWUgfA+4B3gKe86SjmAs8ofqHwF7V96qN5HO9kaOw3fgucG8YBcc8aQxZNqJhP7DK6x2DrU6jWs/GZNa+XXLTK8QHh1HQA/R4VlOQZaXL4XygQzq7gM9j+tbepXqH9KtCLUjbtzpb9U2edHz/7DLfdiZUS3om0Kn6J0CfpzGKPsmR/kxP4z8knQfmqP68Jx2PonyOxk0YWdz4JOBSYAFwuZ4FehZKZw+w1Bvpw77v69Q7qOf7WP2XMArOeCNdQgmkbwNuEqFmkZvujRzD79L/ypP4uEafynmeZAx/ivwh4DvTD6OgtxTpzcBjnhmfpBn8Fjgg9/uzp5WOy4B2xSMtWphSP8LQFUZB0QGNI22vd7fqJ0Us/uoGywU+lcKivHyuMM/57Nyy2fGyy8IoOHucus7lKqduK/BTLcmVQhgFR7UYn7lq+VyhCfjR4XeWtHt6DDr1LmBGiXnqDoWqXUn8XNJ9ii8MNwM7zxVxEd4pHoa3XR/gkrZjZjXwvtrnhHgCYePzoHsMxp3LKQVGLvEex03Xm7DN80GM8KowCk65ekkeMU58OdBbb+Ii3KszP5VwGmkc4tvVbqsncYdwm7q2pxEmwbnEMVnOY7X6v5DHLBuoV4A4YbuTtodRcDrNRNpKF3Fa3qtbbTO8zdOqDtsqIZyFNA7xItk7gCZPa2Jokj1kvyzhrKSLxLc67ari4RQ7W7MQRm7c7m53Aw8AFwGPOK7TxRWq/w0c9qQTw2HZa5D9nXEr+VxhvvbVb8CbdqpM0Y15oaN3K/BaAoVFKocUUNUCJ2Wv2bEfh/G5UX33Wbhqv3CWOuyXvFxioy1WOeBJqsNAzH4cxucV8TPMatDOvV55N8tNHPeGjb6+FtX3e9LqULTXks8VvD0WRsHxMAo6xM94ttnn8aueUpjv3F4OlNCbCIr2pmueoSQb2qS7qeD0cF9dOdLLlC0aVGntUnDtpX0i45CVdHGT/AUc9KRjOpZw7AdW6vaxUu0dJTbaQdmlhM6ESLeqtNUbicmalAb+GlihvhOKzU+ovULyLXHHFEbBiBPgt5IBla60+yovtDl1d2xXqmFEO71ZAVez2iOSt0s/1Hhidq/0Zk5AVtLzVQ7piNyotKxli6bqAtGtlbKdfkT6R9RulfyM9Ds1fmM+V5jlbL5M4UFW0sMqO1R/xsmS2iVhCfCQbvBJOCT5Eic72ig7w04CczhhrIespDfI3TY6Wc9+XRDuKvVHUAz7pL9c45G9Rtnf4I1IQNb89HtypcVEzjuV5JMT8ClwA3CnYh4L9jeHUbAnQddDVtJoZfq93urQo4R6RahFfjoJSxUzZElKVoxKVroSrFNENtX516pmqNdKT46V/wvSdUW9SJsLt/RvMc1WOwD/AGxXRfiY+jewAAAAAElFTkSuQmCC"/>
									</g>
								</svg>
							</div>
							Arabic:
						</h3>
						<ul>
							<li>
								Bader Al Kharafi: 'The role of women in tech needs to be increased. It is that simple.'
							</li>
							<li>
								Released on International Women’s Day, report explores important issues regarding the cause, nature, and effects of the challenges faced...
							</li>
							<li>
								Report finds that if 600 million more women were connected to the internet over the next three years, global GDP would rise between US$13  billion - US$18 billion
							</li>
							<li>
								Continually driving equality programs, Zain pushing an agenda that is cohesive, inclusive, and fair to all
							</li>
							<li>
							Zain tapping into over 100 of its female workforce who were certified in the fundamentals of Data Analytics and Science
							</li>
						</ul>
					</div>
					<div class="description" id="description"  aria-label="DESCRIPTION" role="region" data-color-theme="dark">
						<h3 class="title">
							<div>
								<svg viewBox="0 0 38 38" fill="currentColor">
									<g id="description-icon">
										<image id="Vector_Smart_Object" data-name="Vector Smart Object" width="38" height="38" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAACAUlEQVRYhe2YMUscQRTHfy7XyYE2NoqYQkUbC1ewEywEo1UabfwKLiJp3EIstrKQNSlsrGwUxCoBUfALuBBsFLVICKlFkBRKIvLgLciNzN556+we+INhuXcz+//ve7Nzx2tbHF1BqQJLwDzQD1Rwwz/gGtgFNuIkuhNVT6UHgTNgDRhyaArVGlLts8APxQuSsaqa+mAsKYafwIin5UtN3QOLQKeYdjQ6VfNePYiXpYruqZTPwBeXeYqT6FY0Az+Uj5sanvd0o6fsGCvd8Vy736vZ6LdFudLMpVQ8Y0ZJeDfWKKU1Zjvh+4DZjDn1IOfTHnCTl7GTHH8NZvQh68ZWynYj8nqqja60ZUyecC5jTj1IKbfyNHaqoxBspSwUW8aGgU8Zc5rhP/AN+PHSPWyiR0C3Ec2X5cAPu+Ikeqi9a0uWcspFKV/KVpaxcx2F0JKlHGvigJVDdRu4Mr7JwZi8yl1GtH4mgPEG1zymVbSV8q8RaYzXrD/Q63dbxiab+Nsjpdw3otnI1ukB/thEfwFfjegbEieRlPI3GaUslJY8LpwT+GEv8BE4LJUx4BgYkP3taX8qpcOY6ojADzvUlNDjadMsZaEoYzXa1xXt5K1pYF2vO676GJqphWfawm6pG3fS85zWQBlMTUsfNj3HLsUlsApc1LwQb41oiaZoj8RJdAnwBNM+ae9Pwk73AAAAAElFTkSuQmCC"/>
									</g>
								</svg>
							</div>
							Arabic:
						</h3>
						<div class="contentWrapper">
							<div>
								<p>
									Zain Group, the leading mobile telecom innovator in seven markets across the Middle East and Africa, announces the publication of its annual thought leadership report, this year entitled, Women in Technology - Bridging the Gender Gap in STEM Fields. It provides insights on the gender gap in science, technology, engineering, and math (STEM), highlighting the impact on socio-economic development across the board.
								</p>
								<p>
									Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
								</p>
								<p>
									Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
								</p>
								<p>
									Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
								</p>
							</div>
						</div>
					</div>
						<h3 class="title">
							<div>
								<svg viewBox="0 0 42 47" fill="currentColor">
								<g id="document-icon">
									<image id="Vector_Smart_Object" data-name="Vector Smart Object" width="42" height="47" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAAAvCAYAAACVDljoAAADOUlEQVRYhd2ZSWgUQRSGv8S4BBUUNeAlQWNUXFDIIB4iiBFEwVzixbvLJWQMuGDm4qVBXNBWkKAigqCiEQOCIGhEEVxoEL3kYBKNCEFQXKLkoCby8A12urrH6ZnunsEPCqpfVVf9U9XvVc/rivbGToAFwBFgMzCdZBkHXgKHbce6GjRzJVAPPAO2lUCkUAGsAq6kU5m9Rmu2U3tjZzfQqtffgAGjV7zMAup0hh9Stx1r2DtjFbBF66PAcuBtwkJlRa/rYk0GNgKXvJ1k66u13l8CkdiOJc/oI5dpjtFJt35c6y+A1UaPiUwBtgLLgElGa+GsBTbp3XeAJ8AIcNt2rL6wQhcBt4ClRkt8iLbjwP6qPKdYCdwFaoyWeJHnVyLBYD5CRWQvMFev3wCHgIuu6w7jruJpBtp0lI5/CfWKlNC1HnjnEvoF6DHuzBPbsYI69qRTmRagFmioNJr/siKHyKT4lJ0nSKh4980Si5xA0NY3q5ejItcBxmmRJEFC6131+8ASLWUn1G3foaWkBD2jYfiYxA8IWlE3D4HnhvUP8iJzzrDGQD5CxftPGtaEiWLrEyHfsz6LRINdwFSjpUDSqYz7xiHglO1Yv7yjhRV6XgN/nLwHLnvHD7v1cf9NkZUcNKwFrOhu9fJqoyUahmzHeu03Ulih8oufGtYiyPH2VJRQdDXjcqYRP0eigGdUshXf9fUrjjKcTmUWG7MWILRF/x7ExTygyW/ssFt/QFc1sq33IFHlmmEtQOgDLZGRrzP9t0foQmBnlHHU4/WvgC4/zw8rVIL9BsMaLfJ+a6Qfw2593LmpsaA5wq6obPvZGL3+ne1Y/Ya1AKE/gceGtQjiPEKnaYkEjzN9tR1rzG/csM/ovnI4QmcbrSatMcfemlxHqMSuBk1GnQbuaXLMDzk+D2oKOw7E47v9xhWhJ4Azet3mSvX50aslMsIcoV3AMc3uli1VKlCc5IJ+IZnpyamXBe7w1KdF2FMmQiuNigd3Pqkp5pdlX9KpTK0r9Tma/RbqZb4mA7LeLfXPRq94kWTHDJ3hRtDJNKyh6Khe17k+AybNB/lnkSt4SyTYrt+fShER5ASUmLrGdqyB3w8C3EKyMpgXAAAAAElFTkSuQmCC"/>
								</g>
								</svg>
							</div>
							Arabic:
						</h3>
						<ul class="relatedDocuments__list">
							<li class="relatedDocuments__list--item">
								<a href='#' class="document__info"  aria-label="Image – image-icon – 100 megabytes">
									<h2>
										Mr Ahmed Al Tahous, Chairman Of Zain Group (left) & Bader Al Khafari, Zain Vice Chairman & Group CEO
									</h2>
									<div class="relatedDocuments__list--item--documents">
										<h3> DOWNLOAD IMAGE</h3>
										<div class="icon">
											<span>
												<svg viewbox='0 0 43.94 41.19' fill='currentColor'>
													<g id="gallery-icon">
														<defs><clipPath id="clip-path"><rect class="cls-1" width="43.92" height="42"/></clipPath></defs><g class="cls-2"><path class="cls-3" d="M39,0H5A5,5,0,0,0,0,5V36.22a5,5,0,0,0,5,5H39a5,5,0,0,0,5-5V5a5,5,0,0,0-5-5ZM28.41,7.39a4.81,4.81,0,1,1-4.8,4.81,4.8,4.8,0,0,1,4.8-4.81Zm9,29.2H7.21a1.36,1.36,0,0,1-1.32-2.14l8.24-16.31a1.39,1.39,0,0,1,2.54-.24L25,28.73a2,2,0,0,0,3.14.2l2-2.06a1.93,1.93,0,0,1,3.06.26l5.25,7.49c.75,1.09.3,2-1,2Zm0,0"/></g>
													</g>
												</svg>
											</span>
											100MB
										</div>
									</div>
								</a >
							</li>
							<li class="relatedDocuments__list--item">
								<a href='#' class="document__info"  aria-label="Image – image-icon – 100 megabytes">
									<h2>
										Mr Ahmed Al Tahous, Chairman Of Zain Group (left) & Bader Al Khafari, Zain Vice Chairman & Group CEO
									</h2>
									<div class="relatedDocuments__list--item--documents">
										<h3> DOWNLOAD VIDEO</h3>
										<div class="icon">
											<span>
												<svg viewbox='0 0 238.4 224' fill='currentColor'>
													<g id="video-icon">
														><path d="M238.4,19.2A19.2,19.2,0,0,0,219.2,0H19.2A19.2,19.2,0,0,0,0,19.2V204.8A19.2,19.2,0,0,0,19.2,224h200a19.2,19.2,0,0,0,19.2-19.2ZM72.8,24h34.4V51.2H72.8Zm-24,176H24V172.8H48.8Zm0-148.8H24V24H48.8ZM107.2,200H72.8V172.8h34.4Zm-8.8-65.83V89.83c0-6.6,4.69-9,10.22-5.42l32.18,21c5.53,3.61,5.57,9.53,0,13.14l-32.26,21c-5.52,3.62-10.19,1.18-10.19-5.42ZM165.6,200H131.2V172.8h34.4Zm0-148.8H131.2V24h34.4ZM214.4,200H189.6V172.8h24.8Zm0-148.8H189.6V24h24.8Zm0,0"/>
													</g>
												</svg>
											</span>
											400MB
										</div>
									</div>
								</a >
							</li>
							<li class="relatedDocuments__list--item">
								<a href='#' class="document__info"  aria-label="Image – image-icon – 100 megabytes">
									<h2>
										Mr Ahmed Al Tahous, Chairman Of Zain Group (left) & Bader Al Khafari, Zain Vice Chairman & Group CEO
									</h2>
									<div class="relatedDocuments__list--item--documents">
										<h3> DOWNLOAD ENGLISH PDF</h3>
										<div class="icon">
											<span>
												<svg viewbox='0 0 369 384' fill='currentColor'>
													<g id="pdf-icon">
														<path
															d="M303.37,414.54H605V266.72H303.37ZM510.26,292.13h61.49v22.5h-39v21h31v22.5h-31v32.21h-22.5Zm-88.71,0h35.74a33.08,33.08,0,0,1,33,33V357a33.09,33.09,0,0,1-33,33.05H421.55Zm-86.39,0H371a33,33,0,0,1,0,66h-13.3v32.21h-22.5Zm0,0"
															transform="translate(-236 -106.28)"
														/>
														<path
															d="M381.44,325.11A10.49,10.49,0,0,0,371,314.63h-13.3v21H371a10.5,10.5,0,0,0,10.48-10.49Zm0,0"
															transform="translate(-236 -106.28)"
														/>
														<path
															d="M467.84,357V325.17a10.56,10.56,0,0,0-10.55-10.54H444.06V367.5h13.23A10.56,10.56,0,0,0,467.84,357Zm0,0"
															transform="translate(-236 -106.28)"
														/>
														<path
															d="M280.87,437V244.22H523.25V106.28H342.06v106H236v278H523.25V437Zm88.68-246.26H472.62v22.5H369.55Zm0,0"
															transform="translate(-236 -106.28)"
														/>
														<path
															d="M319.56,106.31,236.07,189.8h83.49Zm0,0"
															transform="translate(-236 -106.28)"
														/>
													</g>
												</svg>
											</span>
											400MB
										</div>
									</div>
								</a >
							</li>
							<li class="relatedDocuments__list--item">
								<a href='#' class="document__info"  aria-label="Image – image-icon – 100 megabytes">
									<h2>
										Mr Ahmed Al Tahous, Chairman Of Zain Group (left) & Bader Al Khafari, Zain Vice Chairman & Group CEO
									</h2>
									<div class="relatedDocuments__list--item--documents">
										<h3> DOWNLOAD ARABIC PDF</h3>
										<div class="icon">
											<span>
												<svg viewbox='0 0 369 384' fill='currentColor'>
													<g id="pdf-icon">
														<path
															d="M303.37,414.54H605V266.72H303.37ZM510.26,292.13h61.49v22.5h-39v21h31v22.5h-31v32.21h-22.5Zm-88.71,0h35.74a33.08,33.08,0,0,1,33,33V357a33.09,33.09,0,0,1-33,33.05H421.55Zm-86.39,0H371a33,33,0,0,1,0,66h-13.3v32.21h-22.5Zm0,0"
															transform="translate(-236 -106.28)"
														/>
														<path
															d="M381.44,325.11A10.49,10.49,0,0,0,371,314.63h-13.3v21H371a10.5,10.5,0,0,0,10.48-10.49Zm0,0"
															transform="translate(-236 -106.28)"
														/>
														<path
															d="M467.84,357V325.17a10.56,10.56,0,0,0-10.55-10.54H444.06V367.5h13.23A10.56,10.56,0,0,0,467.84,357Zm0,0"
															transform="translate(-236 -106.28)"
														/>
														<path
															d="M280.87,437V244.22H523.25V106.28H342.06v106H236v278H523.25V437Zm88.68-246.26H472.62v22.5H369.55Zm0,0"
															transform="translate(-236 -106.28)"
														/>
														<path
															d="M319.56,106.31,236.07,189.8h83.49Zm0,0"
															transform="translate(-236 -106.28)"
														/>
													</g>
												</svg>
											</span>
											400MB
										</div>
									</div>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</section>
		</div>
	</div>
<?php include_once "includes/footer.php";?>