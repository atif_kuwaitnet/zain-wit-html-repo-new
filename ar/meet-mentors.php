<?php $ishome = 1; include_once "includes/header.php";?> 
	<div class="meetMentorsMain">
		<section>
			<div class="mainBanner" role="region" aria-label="mainBanner"> 
				<div class=" banner__slide" role="group">
					<div class="banner__slide--image">
						<img
							src="src/images/banners/mentorsbanner.png"
							alt="main banner image"
						/>
					</div>
					<div class="banner__slide--caption">
						<div class="container">
							<h1>قابل المدربين</h1>
						</div>
					</div>
				</div>
				<a
					href="#"
					class="scrollDown"
					role="button"
					aria-label="Scroll to Contents"
				>
				<span></span>
				</a>
			</div>
		</section>
		<div class="container">
			<div class="meetMentorsMain__slide">
				<div class="meetMentorsMain__slide--head customTabs__items">
					<div class="swiper">
						<div class="swiper-wrapper" role="tablist">
							<div class="swiper-slide meetMentorsMain__slide--link" role="presentation">
								<a
									class="nav-link active"
									data-toggle="tab"
									href="#kuwait"
									role="tab"
									aria-controls="tab_meetMentorsMain_0"
									aria-selected="true"
									id="meetMentorsMain_0"
								>
									<span class="icon">
										<img
											src="src/images/country/kuwait.png"
											alt=""
											class="img-fluid"
										/>
									</span>
									الكويت
								</a>
							</div>
							<div class="swiper-slide meetMentorsMain__slide--link" role="presentation">
								<a
									class="nav-link"
									data-toggle="tab"
									href="#jordan"
									role="tab"
									aria-controls="tab_meetMentorsMain_1"
									aria-selected="false"
									id="meetMentorsMain_1"
								>
									<span class="icon">
										<img
											src="src/images/country/jordan.png"
											alt=""
											class="img-fluid"
										/>
									</span>
									الاردن
								</a>
							</div>
							<div class="swiper-slide meetMentorsMain__slide--link" role="presentation">
								<a
									class="nav-link"
									data-toggle="tab"
									href="#ksa"
									role="tab"
									aria-controls="tab_meetMentorsMain_2"
									aria-selected="false"
									id="meetMentorsMain_2"
								>
									<span class="icon">
										<img
											src="src/images/country/ksa.png"
											alt=""
											class="img-fluid"
										/>
									</span>
									المملكة العربية السعودية
								</a>
							</div>
							<div class="swiper-slide meetMentorsMain__slide--link" role="presentation">
								<a
									class="nav-link"
									data-toggle="tab"
									href="#iraq"
									role="tab"
									aria-controls="tab_meetMentorsMain_3"
									aria-selected="false"
									id="meetMentorsMain_3"
								>
									<span class="icon">
										<img
											src="src/images/country/iraq.png"
											alt=""
											class="img-fluid"
										/>
									</span>
									العراق
								</a>
							</div>
							<div class="swiper-slide meetMentorsMain__slide--link" role="presentation">
								<a
									class="nav-link"
									data-toggle="tab"
									href="#bahrain"
									role="tab"
									aria-controls="tab_meetMentorsMain_4"
									aria-selected="false"
									id="meetMentorsMain_4"
								>
									<span class="icon">
										<img
											src="src/images/country/bahrain.png"
											alt=""
											class="img-fluid"
										/>
									</span>
									البحرين
								</a>
							</div>
							<div class="swiper-slide meetMentorsMain__slide--link" role="presentation">
								<a
									class="nav-link"
									data-toggle="tab"
									href="#sudan"
									role="tab"
									aria-controls="tab_meetMentorsMain_5"
									aria-selected="false"
									id="meetMentorsMain_5"
								>
									<span class="icon">
										<img
											src="src/images/country/sudan.png"
											alt=""
											class="img-fluid"
										/>
									</span>
									السودان
								</a>
							</div>
							<div class="swiper-slide meetMentorsMain__slide--link" role="presentation">
								<a
									class="nav-link"
									data-toggle="tab"
									href="#southSudan"
									role="tab"
									aria-controls="tab_meetMentorsMain_6"
									aria-selected="false"
									id="meetMentorsMain_6"
								>
									<span class="icon">
										<img
											src="src/images/country/south-sudan.png"
											alt=""
											class="img-fluid"
										/>
									</span>
									جنوب السودان
								</a>
							</div>
						</div>
					</div>
					<div class="sliderNavigation side-arrows">
						<div class="container">
							<div class="sliderNavigation__controls">
								<button type="button" role="button" class="sliderNavigation__controls--arrow prevArrow">
									<svg>
										<g id="left-arrow">
											<path
												fill-rule="evenodd"
												d="M0.809,12.572 L20.175,0.476 C20.483,0.287 20.851,0.400 21.066,0.759 C21.282,1.117 21.289,1.628 21.084,1.997 L14.715,13.488 L21.084,24.978 C21.289,25.347 21.282,25.860 21.067,26.216 C20.923,26.460 20.707,26.591 20.487,26.591 C20.382,26.591 20.276,26.561 20.176,26.498 L0.811,14.403 C0.546,14.237 0.377,13.881 0.377,13.488 C0.377,13.094 0.546,12.738 0.809,12.572 Z"
											/>
										</g>
									</svg>
								</button>
								<button type="button" role="button" class="sliderNavigation__controls--arrow nextArrow">
									<svg>
										<g id="right-arrow">
											<path
												fill-rule="evenodd"
												d="M21.190,12.572 L1.825,0.476 C1.517,0.287 1.149,0.400 0.934,0.759 C0.718,1.117 0.711,1.628 0.916,1.997 L7.285,13.488 L0.916,24.978 C0.711,25.347 0.718,25.860 0.933,26.216 C1.077,26.460 1.293,26.591 1.512,26.591 C1.618,26.591 1.724,26.561 1.824,26.498 L21.189,14.403 C21.454,14.237 21.623,13.881 21.623,13.488 C21.623,13.094 21.454,12.738 21.190,12.572 Z"
											/>
										</g>
									</svg>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="meetMentorsMain__detail">
				<div class="tab-content">
					<div class="tab-pane fade show active" role="tabpanel" id="kuwait">
						<div class="row">
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Maysam Al Duaij.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">ميسم الدعيج</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">خبير التحليل الفني للأعمال</span>
									<a href="#" class="abs-link meetMentorsCard__link" data-path="src/data/kuwait/Maysam-AlDuaij.html" data-toggle="custom-modal" data-size="large"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Dana Abdal.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">دانا عبدال</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">أخصائية تطبيقات الرعاية</span>
									<a href="#" class="abs-link meetMentorsCard__link" data-path="src/data/kuwait/Dana-abdal.html" data-toggle="custom-modal" data-size="large"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Nadia Al Saif.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">نادية السيف</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">أخصائي إدارة تطبيقات الرعاية</span>
									<a href="#" class="abs-link meetMentorsCard__link" data-path="src/data/kuwait/Nadia-al-saif.html" data-toggle="custom-modal" data-size="large"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Mohammed Al-Murshed Zain Group.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">محمد المرشد</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">مدير الشبكات</span>
									<a href="#" class="abs-link meetMentorsCard__link" data-path="src/data/kuwait/M.Al-murshad.html" data-toggle="custom-modal" data-size="large"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Mohd Tarawneh Zain Group.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">محمد الطروانة</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">مدير بث الشبكات</span>
									<a href="#" class="abs-link meetMentorsCard__link" data-path="src/data/kuwait/M.Al-tarawneh.html" data-toggle="custom-modal" data-size="large"></a>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" role="tabpanel" id="jordan">
						<div class="row">
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Hiba Afaneh Zain Jordan.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">هبة عفانة</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">إدارة مخاطر المؤسسة والتدقيق الداخلي</span>
									<a href="#" data-path="src/data/jordan/Hiba-Nader-Awad-Afaneh.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Dalia Shakhshir Zain Jordan.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">داليا الشخشير</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">هي قائدة فريق تطوير المنتجات</span>
									<a href="#" data-path="src/data/jordan/Dalia-Mohammed-Ramez-Shakhshir.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Lama Rahwanji Zain Jordan.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">لمى الرهونجي</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">تحتل منصب المدير التنظيمي</span>
									<a href="#" data-path="src/data/jordan/Lama-(Mohammad Said)-Musallam-Al-Rahawanjy.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Alaa-Abdelqader.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">علاء عبد القادر</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination"></span>
									<a href="#" data-path="src/data/jordan/Alaa-Abdelqader.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/yahya-Najjar.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">يحيى النجار</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination"></span>
									<a href="#" data-path="src/data/jordan/yahya-Najjar.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Raad Qandah.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">رعد قندح</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">محلل أداء الشبكات</span>
									<a href="#" data-path="src/data/jordan/Raad-Qandah.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" role="tabpanel" id="ksa">
						<div class="row">
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/unnamed.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">غادة متعب</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">مدير إدارة وتخطيط القوى العاملة</span>
									<a href="#" data-path="src/data/ksa/Ghadah-Mutaab-AlMutairi.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Ali Al Salam Zain KSA.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">علي صالح السلام</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">مدير أول الفواتير وقبول الأصول الثابتة</span>
									<a href="#" data-path="src/data/ksa/Ali-Saleh-Ahmad-Al-Salam.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Bouthina Mahroos Zain KSA.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">بثينة محروس</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">القائم بأعمال مدير الرواتب</span>
									<a href="#" data-path="src/data/ksa/Bouthina-Ahmed-Mahroos.html" data-toggle="custom-modal" data-size="large"  class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Rayan Jan Zain KSA.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">ريان فواز جان</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">مدير أول تطبيقات تخطيط موارد المؤسسات</span>
									<a href="#" data-path="src/data/ksa/Rayan-Fawaz-Jan.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Maryam Abdullatif Alhabab Zain KSA.jpeg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">مريم الحباب</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">مدير الميزانية والرقابة</span>
									<a href="#" data-path="src/data/ksa/Maryam-Abdullatif-Alhabab.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" role="tabpanel" id="iraq">
						<div class="row">
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Haider-Aied-Ali-Alkarway-Zain-Iraq.png" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">حيدر عايد</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">هو رئيس وحدة التقارير المؤسسية </span>
									<a href="#" data-path="src/data/iraq/Haider-Aied-Ali-Alkarway.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Rasha Uday Zain Iraq.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">رشا عدي</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">رئيس قسم تجربة العملاء</span>
									<a href="#" data-path="src/data/iraq/Rasha-Uday-Moain-Moain.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Karim Rahimah Hussein Zain Iraq.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">كريم رحيمة</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">هو رئيس قسم النشر ومتخصص في الهندسة الكهربائي</span>
									<a href="#" data-path="src/data/iraq/Karim-Rahimah-Hussein.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Suhaib Wamidh Dhyaa Eldeen Al Naftaji  Zain Iraq.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">صهيب الدين</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">هو رئيس عمليات تنفيذ الشبكة الأساسية</span>
									<a href="#" data-path="src/data/iraq/Suhaib-Wamidh-Dhyaa-Eldeen-Al-Naftaji.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Ammar-Mohamed-Taher-Al-Shaikly-Zain-Iraq.png" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">عمار طاهر</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">هو رئيس قسم التخطيط والتحسين الأساسي</span>
									<a href="#" data-path="src/data/iraq/Ammar-Mohamed-Taher-Al-Shaikly.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" role="tabpanel" id="bahrain">
						<div class="row">
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Abdulla AlBinAli.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">عبد الله محمد آل بن علي</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">مسؤول تخطيط شبكة الراديو</span>
									<a href="#" data-path="src/data/bahrain/Abdulla-Mohammed-Albinali.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/ali atiah .jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">علي عطية</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">مدير البنية التحتية وخدمة القيمة المضافة والخدمات الأساسية</span>
									<a href="#" data-path="src/data/bahrain/Ali-Atiah.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Mubarak Nass.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">مبارك إبراهيم</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">مسؤول عن عمليات نشر علوم الحاسب والبنية التحتية الأساسية </span>
									<a href="#" data-path="src/data/bahrain/Mubarak-Ebrahim-Nass.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" role="tabpanel" id="sudan">
						<div class="row">
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Abueissa.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">محمد خالد عمران </h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">تكنولوجيا المعلومات والاتصالات</span>
									<a href="#" data-path="src/data/sudan/Mohammed-Khaled-Omran.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Abdelnasir Moutasim.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">عبد الناصر معتصم البشير علي  </h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">مدير تطويرتكنولوجيا المعلومات</span>
									<a href="#" data-path="src/data/sudan/Abdelnasir-Moutasim-Elbashir-Ali.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Ghaidaa.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">غيداء عبد القادر سيرلكاتم  </h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">الإستراتيجية وتخطيط الأعمال</span>
									<a href="#" data-path="src/data/sudan/Ghaidaa-Abdelgadir-Sirelkhatim.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/omer-kamal.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">عمر كمال علي مختار</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">قطاعات التسويق - مدير</span>
									<a href="#" data-path="src/data/sudan/Omer-Kamal Ali-Mukhtar.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Nasreen’s.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">نسرين ابراهيم علي </h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">مدير قسم البيع بالجملة</span>
									<a href="#" data-path="src/data/sudan/Nasreen-Ibraheem-Ali.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" role="tabpanel" id="southSudan">
						<div class="row">
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Chikijwok.jpeg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">شيكيجوك لوال جون</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">متخصص في التجوال والربط البيني</span>
									<a href="#" data-path="src/data/south-sudan/Chikijwok-Lwal-John.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Daniel.jpeg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">دانيل لورو  </h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">رئيس فريق التخطيط والتنفيذ</span>
									<a href="#" data-path="src/data/south-sudan/Daniel-Loro.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Eliza.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">إليزا أيزيا </h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">مديرة العلامات التجارية والاتصالات</span>
									<a href="#" data-path="src/data/south-sudan/Eliza-Isaiah.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Gaudensio.jpeg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">جودنسيو مودي </h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">قائد فريق عمليات الإرسال والإذاعة</span>
									<a href="#" data-path="src/data/south-sudan/Gaudensio-Modi.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Roynina.jpeg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">روينينا ماغايا </h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">مديرة تطبيقات BSS</span>
									<a href="#" data-path="src/data/south-sudan/Roynina-Magaya.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include_once "includes/footer.php";?>