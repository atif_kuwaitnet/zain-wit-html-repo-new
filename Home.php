<?php $ishome = 1; include_once "includes/header.php";?>
    <div class="home">
		<div class="pageSections"> 
            <nav class="navbar bg-faded navbar-light fixed-top navbar-expand" aria-label="Page Navigation">
				<ul  class="nav nav-pills">
					<li class="nav-item">
						<a href="#header" target="_self" class="nav-link"><span >HEADER</span></a>
					</li>
					<li  class="nav-item active">
						<a href="#our_purpose" target="_self" class="nav-link"><span >OUR PURPOSE</span></a>
					</li>
					<li class="nav-item">
						<a href="#programs" target="_self" class="nav-link"><span>PROGRAM</span></a>
					</li>
					<li class="nav-item">
						<a href="#meet_mentors" target="_self" class="nav-link"><span>MEET THE MENTORS</span></a>
					</li>
					<li class="nav-item">
						<a href="#how_join" target="_self" class="nav-link"><span>HOW TO JOIN</span></a>
					</li>
					<li class="nav-item">
						<a href="#faq" target="_self" class="nav-link"><span >FAQs</span></a>
					</li>
					<li class="nav-item">
						<a href="#contact" target="_self" class="nav-link"><span>CONTACT US</span></a>
					</li>
					<li class="nav-item">
						<a href="#footer" target="_self" class="nav-link"><span>FOOTER</span></a>
					</li>
				</ul>
			</nav>
        </div>

		<section>
			<div class="mainBanner" role="region" aria-label="mainBanner">
				<div class="banner__slide" role="group">
					<div class="banner__slide--image">
						<img
							src="src/images/banners/homeBanner-1920x1000.png"
							alt="main banner image"
						/>
					</div>
					
					<div class="banner__slide--caption" >
						<div class="container">
							<h1>Women In Tech</h1>
							<div class="discover" >
								<a href="#" tabindex="-1">
									<span class="icon">
										<svg viewBox="0 0 359.98 192.04" fill="currentColor" >
											<g id="discover-icon">
												<g class="cls-2">
													<path
														d="M75.16,76A48,48,0,1,0,123,124,48,48,0,0,0,75.16,76Zm0,84A36,36,0,1,1,111,124a36,36,0,0,1-35.84,36Zm0,0"
														transform="translate(-3.02 -4)"
													/>
													<path
														d="M332.94,65.52c0-.51.06-1,.06-1.52,0-10-5.26-19.27-14.81-26.09C309.24,31.52,297.46,28,285,28c-17.43,0-33.45,7.08-41.85,18.27a106.71,106.71,0,0,0-18.37-9.92C222.64,18.21,204.72,4,183,4s-39.65,14.21-41.79,32.35a107,107,0,0,0-18.36,9.92C114.45,35.08,98.43,28,81,28c-12.46,0-24.24,3.52-33.19,9.91C38.26,44.73,33,54,33,64c0,.51,0,1,0,1.52a72,72,0,1,0,113.6,65.6,96.29,96.29,0,0,1,72.7,0,72,72,0,1,0,113.59-65.6ZM285,40c16.43,0,30.72,7.8,34.82,18a71.9,71.9,0,0,0-60.66,1.4q-3.12-3.1-6.45-5.92C258.82,45.25,271.22,40,285,40ZM183,16c12.89,0,23.91,6.54,28.14,15.69a109.18,109.18,0,0,0-56.29,0C159.09,22.54,170.11,16,183,16ZM81,40c13.78,0,26.18,5.25,32.29,13.51q-3.33,2.82-6.45,5.92A71.93,71.93,0,0,0,46.18,58C50.28,47.8,64.57,40,81,40ZM75,184a60,60,0,1,1,60-60,60,60,0,0,1-60,60Zm102-71.84a106.59,106.59,0,0,0-30.23,6.07,72,72,0,0,0-29.33-52.35A95.18,95.18,0,0,1,177,40.19V52.76a24,24,0,0,0,0,46.48ZM171,76a12,12,0,1,1,12,12,12,12,0,0,1-12-12Zm48.23,42.23A106.59,106.59,0,0,0,189,112.16V99.24a24,24,0,0,0,0-46.48V40.19a95.23,95.23,0,0,1,59.56,25.69,72,72,0,0,0-29.33,52.35ZM291,184a60,60,0,1,1,60-60,60,60,0,0,1-60,60Zm0,0"
														transform="translate(-3.02 -4)"
													/>
												</g>
												<path
													d="M291.16,76A48,48,0,1,0,339,124a48,48,0,0,0-47.84-48Zm0,84A36,36,0,1,1,327,124a36,36,0,0,1-35.84,36Zm0,0"
													transform="translate(-3.02 -4)"
												/>
											</g>
										</svg>
									</span>
									Discover 
								</a>
							</div>
						</div>
					</div>
				</div>
				<a href="#" class="scrollDown" role="button" aria-label="Scroll to Contents">
					<span></span>
				</a>
				<a href="https://www.youtube.com/watch?v=iJCQbFBELho" target="_zain" class="bannerLink abs-link"></a>
			</div>
		</section>
		<div class="contentWrapper">
			<section data-color-theme="dark" id="our_purpose" class="section-2">
				<div class="about" role="region" aria-label="OUR PURPOSE">
					<div class="container">
						<div class="grid" data-grid-item-width="1/2" data-grid-item-gap="80">
							<div class="about__contents">
								<h2 id="zainpioneer"   class="sectionTitle">OUR PURPOSE</h2>
								<p>
									To foster the development of girls and women in Science<i>,</i>Technology<i>,</i> Engineering and Math (STEM) by connecting them with experienced mentors and motivated mentees. By creating successful matches, the program will enable future generation of girls and women to enter the field with the promise of becoming change-makers in the industry.
								</p>
								
							</div>
							<div class="about__slider">
								<div class="swiper"> 
									<div class="swiper-wrapper" role="tablist">
										<div class="swiper-slide about__slider--slide">
											<img src="src/images/ourpurpose-img.jpg" class="img-fluid" alt="zain image 1"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section data-color-theme="light" id="programs" class="section-3">
				<div class="program" aria-label="Program">
					<div class="container">
						<h2 class="sectionTitle">
							Program
						</h2>
						<p>
							Women in Tech is a mentorship program for women university students seeking guidance on navigating the transition from university to joining the workforce and exploring career options. The mentorship program is based on a human-centered design which is focused on creating a program for women by women.",</p>
							<p>"The design of the program was based on attaining as much feedback from the university students to deeply understand their challenges and needs and how we at Zain can play a proactive role in addressing them. ",</p>
              				<p>" So far, we have conducted 12 focus groups with 3-4 participants in each group and received 860 survey responses across all markets.
						</p>
						<div class="customTabs__items">
							<div class="program__head iconTabs">
								<div class="swiper"> 
									<div class="swiper-wrapper" role="tablist"> 
										<div class="swiper-slide program__item iconTabs__item" >
											<a
												class="nav-link active"
												data-toggle="tab"
												href="#programitem0"
												role="tab"
												aria-controls="tab_zainiac0"
												aria-selected="true"
												id="zainiac0"
											>
												<div class="iconTabs__item--image">
													<span class="tab-icon">
													<img
														src="src/images/icons/white/icon6.png"
														alt=""
														class="img-fluid"
													/>
													<img
														src="src/images/icons/yellow/icon6-active.png"
														alt=""
														class="img-fluid selected"
													/>
													</span>
												</div>
												<h3>Timeline and sessions</h3>
											</a>
										</div>
										<div class="swiper-slide program__item iconTabs__item" >
											<a
												class="nav-link"
												data-toggle="tab"
												href="#programitem1"
												role="tab"
												aria-controls="tab_zainiac1"
												aria-selected="false"
												id="zainiac1"
											>
												<div class="iconTabs__item--image">
													<span class="tab-icon">
													<img
														src="src/images/icons/white/icon5.png"
														alt=""
														class="img-fluid"
													/>
													<img
														src="src/images/icons/yellow/icon5-active.png"
														alt=""
														class="img-fluid selected"
													/>
													</span>
												</div>
												<h3>Experience</h3>
											</a>
										</div>
										<div class="swiper-slide program__item iconTabs__item" >
											<a
												class="nav-link"
												data-toggle="tab"
												href="#programitem2"
												role="tab"
												aria-controls="tab_zainiac2"
												aria-selected="false"
												id="zainiac2"
											>
												<div class="iconTabs__item--image">
													<span class="tab-icon">
													<img
														src="src/images/icons/white/icon2.png"
														alt=""
														class="img-fluid"
													/>
													<img
														src="src/images/icons/yellow/icon2-active.png"
														alt=""
														class="img-fluid selected"
													/>
													</span>
												</div>
												<h3>Benefits of Participation</h3>
											</a>
										</div>
										<div class="swiper-slide program__item iconTabs__item" >
											<a
												class="nav-link"
												data-toggle="tab"
												href="#programitem3"
												role="tab"
												aria-controls="tab_zainiac3"
												aria-selected="false"
												id="zainiac3"
											>
												<div class="iconTabs__item--image">
													<span class="tab-icon">
													<img
														src="src/images/icons/white/icon3.png"
														alt=""
														class="img-fluid"
													/>
													<img
														src="src/images/icons/yellow/icon3-active.png"
														alt=""
														class="img-fluid selected"
													/>
													</span>
												</div>
												<h3>Responsibility of the Mentees</h3>
											</a>
										</div>
										<div class="swiper-slide program__item iconTabs__item" >
											<a
												class="nav-link"
												data-toggle="tab"
												href="#programitem4"
												role="tab"
												aria-controls="tab_zainiac4"
												aria-selected="false"
												id="zainiac4"
											>
												<div class="iconTabs__item--image">
													<span class="tab-icon">
													<img
														src="src/images/icons/white/icon4.png"
														alt=""
														class="img-fluid"
													/>
													<img
														src="src/images/icons/yellow/icon4-active.png"
														alt=""
														class="img-fluid selected"
													/>
													</span>
												</div>
												<h3>WiT community</h3>
											</a>
										</div>
									</div>
								</div>
								<div class="sliderNavigation__custom">
									<div class="sliderNavigation sideNavigation navigationSimple">
										<div class="container">
											<div class="sliderNavigation__controls">
												<button type="button" role="button" class="sliderNavigation__controls--arrow prevArrow">
													<svg fill="currentColor">
														<g id="left-arrow">
															<path
																fill-rule="evenodd"
																d="M0.809,12.572 L20.175,0.476 C20.483,0.287 20.851,0.400 21.066,0.759 C21.282,1.117 21.289,1.628 21.084,1.997 L14.715,13.488 L21.084,24.978 C21.289,25.347 21.282,25.860 21.067,26.216 C20.923,26.460 20.707,26.591 20.487,26.591 C20.382,26.591 20.276,26.561 20.176,26.498 L0.811,14.403 C0.546,14.237 0.377,13.881 0.377,13.488 C0.377,13.094 0.546,12.738 0.809,12.572 Z"
															/>
														</g>
													</svg>
												</button>
												<button type="button" role="button" class="sliderNavigation__controls--arrow nextArrow">
													<svg fill="currentColor">
														<g id="right-arrow">
															<path
																fill-rule="evenodd"
																d="M21.190,12.572 L1.825,0.476 C1.517,0.287 1.149,0.400 0.934,0.759 C0.718,1.117 0.711,1.628 0.916,1.997 L7.285,13.488 L0.916,24.978 C0.711,25.347 0.718,25.860 0.933,26.216 C1.077,26.460 1.293,26.591 1.512,26.591 C1.618,26.591 1.724,26.561 1.824,26.498 L21.189,14.403 C21.454,14.237 21.623,13.881 21.623,13.488 C21.623,13.094 21.454,12.738 21.190,12.572 Z"
															/>
														</g>
													</svg>
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="program__body">
								<div class="tab-content">
									<div class="tab-pane fade show active" id="programitem0" role="tabpanel">
										<div class="howtoLearn__content">
											<div class="mb-40 text-center">
												<span class="sectionSubTitle">
													Application closing date: September 2
												</span>
											</div>
											<ul>
												<li>
													<p>
														Introductory Session/ Meet and Greet: September 13
													</p>
												</li>
												<li>
													<p>
														Session 1: Week of September 19
													</p>
												</li>
												<li>
													<p>
														Session 2: Week of October 17
													</p>
												</li>
												<li>
													<p>
														Session 3: Week of October 31
													</p>
												</li>
												<li>
													<p>
														Focus Group Session- We want to hear from you!
													</p>
												</li>
												<li>
													<p>
														Session 4: Week of November 14
													</p>
												</li>
												<li>
													<p>
														Session 5: Week of November 21
													</p>
												</li>
												<li>
													<p>
														Session 6: Week of November 28
													</p>
												</li>
												<li>
													<p>
														Focus Group Session – We want to hear from you
													</p>
												</li>
												<li>
													<p>
														Closing Ceremony
													</p>
												</li>
											</ul>
											
										</div>
									</div>
									<div class="tab-pane fade" id="programitem1" role="tabpanel">
										<div class="Experience__content">
											<ul>
												<li>
													<p>STEM Background</p>
												</li>
												<li>
													<p>Level of experience manager and above</p>
												</li>
												<li>
													<p>Varied Leadership experience</p>
												</li>
												<li>
													<p>Possess the following skills: listening, advocacy, teaching and sharing</p>
												</li>
												<li>
													<p>Have a passion to inspire and guide future tech-leaders</p>
												</li>
											</ul>
										</div>
									</div>

									<div class="tab-pane fade" id="programitem2" role="tabpanel">
										<div class="benefits__content">
											<div class="row">
												<div class="col-md-6">
												<div class="image">
													<img
													src="src/images/home/TabImage.png"
													class="img-fluid"
													alt=""
													/>
												</div>
												</div>
												<div class="col-md-6">
													<h3 class="tabHeading">
														what to expect?
													</h3>
													<ul>
														<li>
															<p>Challenge, motivate and inspire women who have chosen to be part of the STEM industry</p>
														</li>
														<li>
															<p>Gain firsthand exposure with experts in the STEM industry</p>
														</li>
														<li>
															<p>Create a network of future tech leaders in the region</p>
														</li>
														<li>
															<p>Provide women tools on how to combat gender biases</p>
														</li>
														<li>
															<p>Explore career opportunities and gain guidance by sharing experiences</p>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>

									<div class="tab-pane fade" id="programitem3" role="tabpanel">
										<div class="responsibility__content">
											<div class="swiper"> 
												<div class="swiper-wrapper" role="tablist"> 
													<div class="swiper-slide" role="presentation">
														<div class="responsibility__content--item">
															<img src="src/images/icons/about-icons/yellow/Calendar.png" class="img-fluid" alt="" />
															<p class="title">Schedule Meetings</p>
														</div>
													</div>
													<div class="swiper-slide" role="presentation">
														<div class="responsibility__content--item">
															<img src="src/images/icons/about-icons/yellow/setting.png" class="img-fluid" alt="" />
															<p class="title">Goal Setting (SMART)</p>
														</div>
													</div>
													<div class="swiper-slide" role="presentation">
														<div class="responsibility__content--item">
															<img src="src/images/icons/about-icons/yellow/round-table.png" class="img-fluid" alt="" />
															<p class="title">Prepare for Meetings</p>
														</div>
													</div>
													<div class="swiper-slide" role="presentation">
														<div class="responsibility__content--item">
															<img src="src/images/icons/about-icons/yellow/be-open.png" class="img-fluid" alt="" />
															<p class="title">Be Open, Honest & Respectful</p>
														</div>
													</div>
													<div class="swiper-slide" role="presentation">
														<div class="responsibility__content--item">
															<img src="src/images/icons/about-icons/yellow/safe.png" class="img-fluid" alt="" />
															<p class="title">Safe Space</p>
														</div>
													</div>
													<div class="swiper-slide" role="presentation">
														<div class="responsibility__content--item">
															<img src="src/images/icons/about-icons/yellow/secret-file.png" class="img-fluid" alt="" />
															<p class="title">Confidentiality – but if any concerns rise please inform WiT team</p>
														</div>
													</div>
													<div class="swiper-slide" role="presentation">
														<div class="responsibility__content--item">
															<img src="src/images/icons/about-icons/yellow/attend.png" class="img-fluid" alt="" />
															<p class="title">Attend all Focus Groups</p>
														</div>
													</div>
													<div class="swiper-slide" role="presentation">
														<div class="responsibility__content--item">
															<img src="src/images/icons/about-icons/yellow/call-center.png" class="img-fluid" alt="" />
															<p class="title">Complete 3 Surveys</p>
														</div>
													</div>
												</div>
											</div>
											<div class="sliderNavigation__custom">
												<div class="sliderNavigation sideNavigation navigationSimple">
													<div class="container">
														<div class="sliderNavigation__control">
															<button type="button" role="button" class="sliderNavigation__control--arrow prevArrow">
																<svg fill="currentColor">
																	<g id="left-arrow">
																		<path
																			fill-rule="evenodd"
																			d="M0.809,12.572 L20.175,0.476 C20.483,0.287 20.851,0.400 21.066,0.759 C21.282,1.117 21.289,1.628 21.084,1.997 L14.715,13.488 L21.084,24.978 C21.289,25.347 21.282,25.860 21.067,26.216 C20.923,26.460 20.707,26.591 20.487,26.591 C20.382,26.591 20.276,26.561 20.176,26.498 L0.811,14.403 C0.546,14.237 0.377,13.881 0.377,13.488 C0.377,13.094 0.546,12.738 0.809,12.572 Z"
																		/>
																	</g>
																</svg>
															</button>
															<button type="button" role="button" class="sliderNavigation__control--arrow nextArrow">
																<svg fill='currentColor'>
																	<g id="right-arrow">
																		<path
																			fill-rule="evenodd"
																			d="M21.190,12.572 L1.825,0.476 C1.517,0.287 1.149,0.400 0.934,0.759 C0.718,1.117 0.711,1.628 0.916,1.997 L7.285,13.488 L0.916,24.978 C0.711,25.347 0.718,25.860 0.933,26.216 C1.077,26.460 1.293,26.591 1.512,26.591 C1.618,26.591 1.724,26.561 1.824,26.498 L21.189,14.403 C21.454,14.237 21.623,13.881 21.623,13.488 C21.623,13.094 21.454,12.738 21.190,12.572 Z"
																		/>
																	</g>
																</svg>
															</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="tab-pane fade" id="programitem4" role="tabpanel">
										<div class="ZainWorld" id="worldofzain">
											<div class="inner-scroll">
												<p>The program spans across the Middle East and North Africa and is provided in seven countries which include Bahrain, Iraq, Kuwait, Jordan, Saudi Arabia, Sudan and South Sudan. When it comes to the region as a whole, there is still a long way to go in addressing the gap in STEM. At Zain, we are committed to bridging the divide as it will allow women to contribute in the future of technology.</p>
											</div>
											<div class="ZainWorld__slide customTabs">
												<div class="ZainWorld__slide--head customTabs__items"> 
													<div class="swiper"> 
														<div class="swiper-wrapper" role="tablist">
															<div class="swiper-slide ZainWorld__slide--link" role="presentation">
																<a
																	class="nav-link active"
																	data-toggle="tab"
																	href="#zainWorld00"
																	role="tab"
																	aria-controls="tab_zainWorld_0"
																	aria-selected="true"
																	id="zainWorld_0"
																>
																	<span class="icon">
																		<img src="src/images/country/kuwait.png" alt="" class="img-fluid"/>
																	</span>
																	kuwait
																</a>
															</div>
															<div class="swiper-slide ZainWorld__slide--link" role="presentation">
																<a
																	class="nav-link"
																	data-toggle="tab"
																	href="#zainWorld01"
																	role="tab"
																	aria-controls="tab_zainWorld_1"
																	aria-selected="false"
																	id="zainWorld_1"
																>
																	<span class="icon">
																		<img src="src/images/country/jordan.png" alt="" class="img-fluid"/>
																	</span>
																	jordan
																</a>
															</div>
															<div class="swiper-slide ZainWorld__slide--link" role="presentation">
																<a
																	class="nav-link"
																	data-toggle="tab"
																	href="#zainWorld02"
																	role="tab"
																	aria-controls="tab_zainWorld_2"
																	aria-selected="false"
																	id="zainWorld_2"
																>
																	<span class="icon">
																		<img src="src/images/country/ksa.png" alt="" class="img-fluid"/>
																	</span>
																	Saudia Arabia
																</a>
															</div>
															<div class="swiper-slide ZainWorld__slide--link" role="presentation">
																<a
																	class="nav-link"
																	data-toggle="tab"
																	href="#zainWorld03"
																	role="tab"
																	aria-controls="tab_zainWorld_3"
																	aria-selected="false"
																	id="zainWorld_3"
																>
																	<span class="icon">
																		<img src="src/images/country/south-sudan.png" alt="" class="img-fluid"/>
																	</span>
																	SOUTH SUDAN
																</a>
															</div>
															<div class="swiper-slide ZainWorld__slide--link" role="presentation">
																<a
																	class="nav-link"
																	data-toggle="tab"
																	href="#zainWorld04"
																	role="tab"
																	aria-controls="tab_zainWorld_4"
																	aria-selected="false"
																	id="zainWorld_4"
																>
																	<span class="icon">
																		<img src="src/images/country/sudan.png" alt="" class="img-fluid"/>
																	</span>
																	SUDAN
																</a>
															</div>
															<div class="swiper-slide ZainWorld__slide--link" role="presentation">
																<a
																	class="nav-link"
																	data-toggle="tab"
																	href="#zainWorld05"
																	role="tab"
																	aria-controls="tab_zainWorld_5"
																	aria-selected="false"
																	id="zainWorld_5"
																>
																	<span class="icon">
																		<img src="src/images/country/iraq.png" alt="" class="img-fluid"/>
																	</span>
																	Iraq
																</a>
															</div>
															<div class="swiper-slide ZainWorld__slide--link" role="presentation">
																<a
																	class="nav-link"
																	data-toggle="tab"
																	href="#zainWorld06"
																	role="tab"
																	aria-controls="tab_zainWorld_6"
																	aria-selected="false"
																	id="zainWorld_6"
																>
																	<span class="icon">
																		<img src="src/images/country/uae.png" alt="" class="img-fluid"/>
																	</span>
																	uae
																</a>
															</div>
														</div>
													</div>
													
													<div class="sliderNavigation side-arrows">
														<div class="container">
															<div class="sliderNavigation__control">
																<button type="button" role="button" class="sliderNavigation__control--arrow prevArrow">
																	<svg>
																		<g id="left-arrow">
																			<path
																				fill-rule="evenodd"
																				d="M0.809,12.572 L20.175,0.476 C20.483,0.287 20.851,0.400 21.066,0.759 C21.282,1.117 21.289,1.628 21.084,1.997 L14.715,13.488 L21.084,24.978 C21.289,25.347 21.282,25.860 21.067,26.216 C20.923,26.460 20.707,26.591 20.487,26.591 C20.382,26.591 20.276,26.561 20.176,26.498 L0.811,14.403 C0.546,14.237 0.377,13.881 0.377,13.488 C0.377,13.094 0.546,12.738 0.809,12.572 Z"
																			/>
																		</g>
																	</svg>
																</button>
																<button type="button" role="button" class="sliderNavigation__control--arrow nextArrow">
																	<svg>
																		<g id="right-arrow">
																			<path
																				fill-rule="evenodd"
																				d="M21.190,12.572 L1.825,0.476 C1.517,0.287 1.149,0.400 0.934,0.759 C0.718,1.117 0.711,1.628 0.916,1.997 L7.285,13.488 L0.916,24.978 C0.711,25.347 0.718,25.860 0.933,26.216 C1.077,26.460 1.293,26.591 1.512,26.591 C1.618,26.591 1.724,26.561 1.824,26.498 L21.189,14.403 C21.454,14.237 21.623,13.881 21.623,13.488 C21.623,13.094 21.454,12.738 21.190,12.572 Z"
																			/>
																		</g>
																	</svg>
																</button>
															</div>
														</div>
													</div>
												</div>
												<div class="ZainWorld__slide--body customTabs__contents">
													<div class="tab-content">
														<div class="tab-pane fade show active" id="zainWorld00" role="tabpanel">
															<div class="ZainWorld__content">
																<div class="ZainWorld__content--world text-center">
																	<img src="src/images/map/KUWAIT_MAP.png" alt="" class="img-fluid" />
																</div>
															</div>
														</div>
														<div class="tab-pane fade" id="zainWorld01" role="tabpanel">
															<div class="ZainWorld__content">
																<div class="ZainWorld__content--world text-center">
																	<img src="src/images/map/JORDAN_MAP.png" alt="" class="img-fluid" />
																</div>
															</div>
														</div>
														<div class="tab-pane fade" id="zainWorld02" role="tabpanel">
															<div class="ZainWorld__content">
																<div class="ZainWorld__content--world text-center">
																	<img src="src/images/map/SAUDI-ARABIA_MAP.png" alt="" class="img-fluid" />
																</div>
															</div>
														</div>
														<div class="tab-pane fade" id="zainWorld03" role="tabpanel">
															<div class="ZainWorld__content">
																<div class="ZainWorld__content--world text-center">
																	<img src="src/images/map/SOUTH-SUDAN_MAP.png" alt="" class="img-fluid" />
																</div>
															</div>
														</div>
														<div class="tab-pane fade" id="zainWorld04" role="tabpanel">
															<div class="ZainWorld__content">
																<div class="ZainWorld__content--world text-center">
																	<img src="src/images/map/SUDAN_MAP.png" alt="" class="img-fluid" />
																</div>
															</div>
														</div>
														<div class="tab-pane fade" id="zainWorld05" role="tabpanel">
															<div class="ZainWorld__content">
																<div class="ZainWorld__content--world text-center">
																	<img src="src/images/map/IRAQ_MAP.png" alt="" class="img-fluid" />
																</div>
															</div>
														</div>
														<div class="tab-pane fade" id="zainWorld06" role="tabpanel">
															<div class="ZainWorld__content">
																<div class="ZainWorld__content--world text-center">
																	<img src="src/images/map/Map-UAE_Map.png" alt="" class="img-fluid" />
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section data-color-theme="dark" id="meet_mentors" class="section-4">
				<div class="meetmentors" aria-label="meetmentors" role="region">
					<div class="container">
						<div class="topHead">
							<h2 class="sectionTitle" id="newslatter">Meet the Mentors</h2>
							<a class="btn btn-primary" href="meet-mentors.php">view all</a>
						</div>
					</div>
					<div class="meetmentorsWrapper">
						<ul class="slideAccordion">
							<li class="slideAccordion__items" tabIndex="0">
								<div class="slideAccordion__items--bgLayer"></div>
								<div class="slideAccordion__items--image">
									<img src="src/images/mentors/Bouthina Mahroos Zain KSA.jpg" alt="">
								</div>
								<div class="slideAccordion__items--content">
									<h2 id="edition0">Bouthina Ahmed Mahroos</h2>
									<h3>Acting Payroll Senior Manager</h3>
									<p class="country">
										<span class="icon">
											<img src="src/images/country/ksa.png" alt="">
										</span>
										ksa
									</p>
									<div class="detailInfo">
										<p> Bouthina Ahmed Mahroos has 13 years-experience in HR, and worked in different industries including FMCG, automotive, financial services and telecommunication. She strives to enjoy the journey and create a balance between work and personal life. What drives Bouthina is ‘work hard until you don’t have to introduce yourself’.</p>
									</div>
									<a href="#" data-path="src/data/ksa/Bouthina-Ahmed-Mahroos.html" data-toggle="custom-modal" data-size="large" aria-describedby="edition0" role="edition0" class="btn btn-yellow">explore more</a>
								</div>
							</li>
							<li class="slideAccordion__items" tabIndex="0">
								<div class="slideAccordion__items--bgLayer"></div>
								<div class="slideAccordion__items--image">
									<img src="src/images/mentors/Dana Abdal.jpg" alt="">
								</div>
								<div class="slideAccordion__items--content">
									<h2 id="edition1">DANA ABDAL</h2>
									<h3>Care Applications Management Specialist</h3>
									<p class="country">
										<span class="icon">
											<img src="src/images/country/kuwait.png" alt="">
										</span>
										kuwait
									</p>
									<div class="detailInfo">
										<p> Dana Abdal is a Care Apps Specialist at Zain Kuwait. She has been with Zain Kuwait IT department for over 10 years and has worked on diverse IT projects which fostered the development of her knowledge and experience in the field today. She is driven by the phrase 'Never look down, unless you are helping someone up'. Dana also enjoys playing the piano and was the first to introduce the art of reborning in the Arab region.</p>									
									</div>
									<a href="#"  data-path="src/data/kuwait/Dana-abdal.html" data-toggle="custom-modal" data-size="large" aria-describedby="edition1" role="edition1" class="btn btn-yellow">explore more</a>
								</div>
							</li>
							<li class="slideAccordion__items" tabIndex="0">
								<div class="slideAccordion__items--bgLayer"></div>
								<div class="slideAccordion__items--image">
									<img src="src/images/mentors/Dalia Shakhshir Zain Jordan.jpg" alt="">
								</div>
								<div class="slideAccordion__items--content">
									<h2 id="edition2">Dalia Mohammed Ramez Shakhshir</h2>
									<h3>Consumer Business</h3>
									<p class="country">
										<span class="icon">
											<img src="src/images/country/jordan.png" alt="">
										</span>
										jordan
									</p>
									<div class="detailInfo">
										<p> Dalia Shakhshir is a passionate and dedicated Product Development Team Leader in the Telecom domain with a proven track of quality deliverables, IT development, and Business Analysis experience of 16+ years. Dalia is a creative detail-oriented problem solver, with advanced analytical and reasoning skills who consistently delivers and fulfils strategic implementations. Honesty and kindness are the main values Dalia believes in and lives by. Spending quality time with her three children is what Dalia enjoys the most, as well as walking and making homemade sweets.</p>
									</div>
									<a href="#" data-path="src/data/jordan/Dalia-Mohammed-Ramez-Shakhshir.html" data-toggle="custom-modal" data-size="large" aria-describedby="edition2" role="edition2" class="btn btn-yellow">explore more</a>
								</div>
							</li>
							<li class="slideAccordion__items" tabIndex="0">
								<div class="slideAccordion__items--bgLayer"></div>
								<div class="slideAccordion__items--image">
									<img src="src/images/mentors/Haider-Aied-Ali-Alkarway-Zain-Iraq.png" alt="">
								</div>
								<div class="slideAccordion__items--content">
									<h2 id="edition3">Haider Aied Ali Alkarway</h2>
									<h3>Head of Corporate Reporting Unit</h3>
									<p class="country">
										<span class="icon">
											<img src="src/images/country/iraq.png" alt="">
										</span>
										iraq
									</p>
									<div class="detailInfo">
										<p> Haider Aied is the Head of the Corporate Reporting Unit since 2017 with over 15 years’ experience at Zain Iraq. He is responsible for communicating with different departments to support the creation of reports and dashboards. Haider enjoys hunting and camping.</p>
									</div>
									<a href="#" data-path="src/data/iraq/Haider-Aied-Ali-Alkarway.html" data-toggle="custom-modal" data-size="large" aria-describedby="edition3" role="edition3" class="btn btn-yellow">explore more</a>
								</div>
							</li>
							<li class="slideAccordion__items" tabIndex="0">
								<div class="slideAccordion__items--bgLayer"></div>
								<div class="slideAccordion__items--image">
									<img src="src/images/mentors/omer-kamal.jpg" alt="">
								</div>
								<div class="slideAccordion__items--content">
									<h2 id="edition4">Omer Kamal Ali Mukhtar</h2>
									<h3>Telecom Management professional</h3>
									<p class="country">
										<span class="icon">
											<img src="src/images/country/sudan.png" alt="">
										</span>
										Sudan
									</p>
									<div class="detailInfo">
										<p> Omer is a Qualified Telecom Management Professional with over 17 years of experience in Marketing, Technology, Project Management, and Customer Experience Management. He has the Proficiency of implementing strategies that consistently enhancing business and operational targets as well as accomplishing customer satisfaction targets across professional career. Omer played an excellent role through different roles in Zain starting from Technology Operations then Technology Planning, and also when Managing Customer Experience Delivery team and Finally in Marketing roles as a Manager and a Director.</p><br/><p>Also Omer had Proven ability of development & implementation of departmental strategy & plans for accomplishing departmental & organizational business objectives; setting up improvement initiatives based on structured methods in assessment, for accomplishing Organizational targets and  Objectives.</p>
									</div>
									<a href="#" data-path="src/data/sudan/Omer-Kamal Ali-Mukhtar.html" data-toggle="custom-modal" data-size="large" aria-describedby="edition4" role="edition4" class="btn btn-yellow">explore more</a>
								</div>
							</li>
							<li class="slideAccordion__items" tabIndex="0">
								<div class="slideAccordion__items--bgLayer"></div>
								<div class="slideAccordion__items--image">
									<img src="src/images/mentors/Mubarak Nass.jpg" alt="">
								</div>
								<div class="slideAccordion__items--content">
									<h2 id="edition5">Mubarak Ebrahim Nass</h2>
									<h3>Infrastructure & Core Specialist</h3>
									<p class="country">
										<span class="icon">
											<img src="src/images/country/bahrain.png" alt="">
										</span>
										bahrain
									</p>
									<div class="detailInfo">
										<p> Mubarak Ebrahim Nass is Infrastructure & Core Specialist at Zain Bahrain who is responsible for CS Core & Infrastructure deployments, IT help desk & ERP. Mubarak is blessed to be part of many success stories at Zain Bahrain like 4G launch, 5G launch, and most recent being the Apple iWatch enablement & launch. Mubarak is passionate about looking for new technologies and methods to enhance the customer & end-user experience. Mubarak likes to play video games, read & watch movies.</p>
									</div>
									<a href="#" data-path="src/data/bahrain/Mubarak-Ebrahim-Nass.html" data-toggle="custom-modal" data-size="large" aria-describedby="edition5" role="edition5" class="btn btn-yellow">explore more</a>
								</div>
							</li>
							<li class="slideAccordion__items" tabIndex="0">
								<div class="slideAccordion__items--bgLayer"></div>
								<div class="slideAccordion__items--image">
									<img src="src/images/mentors/Eliza.jpg" alt="">
								</div>
								<div class="slideAccordion__items--content">
									<h2 id="edition6">Eliza Isaiah</h2>
									<h3>Branding and Communication Manager</h3>
									<p class="country">
										<span class="icon">
											<img src="src/images/country/south-sudan.png" alt="">
										</span>
										South Sudan
									</p>
									<div class="detailInfo">
										<p> Eliza Isaiah is the Branding and Communication Manager at Zain South Sudan. </p>
									</div>
									<a href="#" data-path="src/data/south-sudan/Eliza-Isaiah.html" data-toggle="custom-modal" data-size="large" aria-describedby="edition6" role="edition6" class="btn btn-yellow">explore more</a>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</section>
			<section id="how_join" data-color-theme="dark" class="section-5">
				<div class="howJoin" aria-label="how to join?">
					<div class="container">
						<h2 class="sectionTitle">how to join?</h2>
						<div class="tabs">
							<ul class="nav nav-pills" id="pills-tab" role="tablist">
								<li class="nav-item" >
									<a class="nav-link active" data-toggle="pill" href="#required" role="tab" aria-controls="pills-required" aria-selected="true">Applicant Requirements</a>
								</li>
								<li class="nav-item" >
									<a class="nav-link" data-toggle="pill" href="#applicant" role="tab" aria-controls="pills-applicant" aria-selected="false">Applicants must be</a>
								</li>
							</ul>
							<div class="tab-content mt-20">
								<div class="tab-pane fade show active innerTab" id="required" role="tabpanel" aria-labelledby="pills-required-tab">
									<div class="applicant__requirment required">
										<p>The program is open for women university students that are studying fields under STEM (Science, Technology, Engineering and Math). Majors include and not limited to:</p>
										<div class="sliderNavigation__custom">
											<div class="sliderNavigation navigationSimple">
												<div class="container">
													<div class="sliderNavigation__controls">
														<button type="button" role="button" class="sliderNavigation__controls--arrow prevArrow">
															<svg fill="currentColor">
															<g id="left-arrow">
																<path
																	fill-rule="evenodd"
																	d="M0.809,12.572 L20.175,0.476 C20.483,0.287 20.851,0.400 21.066,0.759 C21.282,1.117 21.289,1.628 21.084,1.997 L14.715,13.488 L21.084,24.978 C21.289,25.347 21.282,25.860 21.067,26.216 C20.923,26.460 20.707,26.591 20.487,26.591 C20.382,26.591 20.276,26.561 20.176,26.498 L0.811,14.403 C0.546,14.237 0.377,13.881 0.377,13.488 C0.377,13.094 0.546,12.738 0.809,12.572 Z"
																/>
															</g>
															</svg>
														</button>
														<button type="button" role="button" class="sliderNavigation__controls--arrow nextArrow">
															<svg fill="currentColor">
															<g id="right-arrow">
																<path
																	fill-rule="evenodd"
																	d="M21.190,12.572 L1.825,0.476 C1.517,0.287 1.149,0.400 0.934,0.759 C0.718,1.117 0.711,1.628 0.916,1.997 L7.285,13.488 L0.916,24.978 C0.711,25.347 0.718,25.860 0.933,26.216 C1.077,26.460 1.293,26.591 1.512,26.591 C1.618,26.591 1.724,26.561 1.824,26.498 L21.189,14.403 C21.454,14.237 21.623,13.881 21.623,13.488 C21.623,13.094 21.454,12.738 21.190,12.572 Z"
																/>
															</g>
															</svg>
														</button>
													</div>
												</div>
											</div>
										</div>
										<div class="swiper requirment"> 
											<div class="swiper-wrapper"> 
												<div class="swiper-slide">
													<div class="nav-link requirment__slide--link">
														<div class="iconTabs__item--image">
															<span class="tab-icon">
																<img src="src/images/icons/howJoin/Icons-Zain Women In Tech_Computer_Science.png" alt="" class="img-fluid"/>
															</span>
														</div>
														<span class="requirment__slide--count">01</span>
														<h3 class="requirment__slide--title">Computer Science</h3>
													</div>
												</div>
												<div class="swiper-slide">
													<div class="nav-link requirment__slide--link">
														<div class="iconTabs__item--image">
															<span class="tab-icon">
																<img src="src/images/icons/howJoin/Icons-Zain Women In Tech_Information-Technology.png" alt="" class="img-fluid"/>
															</span>
														</div>
														<span class="requirment__slide--count">02</span>
														<h3 class="requirment__slide--title">Information Technology</h3>
													</div>
												</div>
												<div class="swiper-slide">
													<div class="nav-link requirment__slide--link">
														<div class="iconTabs__item--image">
															<span class="tab-icon">
																<img src="src/images/icons/howJoin/Icons-Zain Women In Tech_Mechanical_Engineereing.png" alt="" class="img-fluid"/>
															</span>
														</div>
														<span class="requirment__slide--count">03</span>
														<h3 class="requirment__slide--title">Mechanical Engineering</h3>
													</div>
												</div>
												<div class="swiper-slide">
													<div class="nav-link requirment__slide--link">
														<div class="iconTabs__item--image">
															<span class="tab-icon">
																<img src="src/images/icons/howJoin/Icons-Zain Women In Tech_Data_Analystic.png" alt="" class="img-fluid"/>
															</span>
														</div>
														<span class="requirment__slide--count">04</span>
														<h3 class="requirment__slide--title">Data Analytics</h3>
													</div>
												</div>
												<div class="swiper-slide">
													<div class="nav-link requirment__slide--link">
														<div class="iconTabs__item--image">
															<span class="tab-icon">
																<img src="src/images/icons/howJoin/Icons-Zain Women In Tech_Graphic-Web_Design.png" alt="" class="img-fluid"/>
															</span>
														</div>
														<span class="requirment__slide--count">05</span>
														<h3 class="requirment__slide--title">Graphic/Web Design </h3>
													</div>
												</div>
												<div class="swiper-slide">
													<div class="nav-link requirment__slide--link">
														<div class="iconTabs__item--image">
															<span class="tab-icon">
																<img src="src/images/icons/howJoin/Icons-Zain Women In Tech_Cyber_Security.png" alt="" class="img-fluid"/>
															</span>
														</div>
														<span class="requirment__slide--count">06</span>
														<h3 class="requirment__slide--title">Cyber Security</h3>
													</div>
												</div>
												<div class="swiper-slide">
													<div class="nav-link requirment__slide--link">
														<div class="iconTabs__item--image">
															<span class="tab-icon">
																<img src="src/images/icons/howJoin/Icons-Zain Women In Tech_Chemical_Engenireeing.png" alt="" class="img-fluid"/>
															</span>
														</div>
														<span class="requirment__slide--count">07</span>
														<h3 class="requirment__slide--title">Chemical Engineering</h3>
													</div>
												</div>
												<div class="swiper-slide">
													<div class="nav-link requirment__slide--link">
														<div class="iconTabs__item--image">
															<span class="tab-icon">
																<img src="src/images/icons/howJoin/Icons-Zain Women In Tech_Electrical_Engineering.png" alt="" class="img-fluid"/>
															</span>
														</div>
														<span class="requirment__slide--count">08</span>
														<h3 class="requirment__slide--title">Electrical Engineering</h3>
													</div>
												</div>
												<div class="swiper-slide">
													<div class="nav-link requirment__slide--link">
														<div class="iconTabs__item--image">
															<span class="tab-icon">
																<img src="src/images/icons/howJoin/Icons-Zain Women In Tech_Software_Engineering.png" alt="" class="img-fluid"/>
															</span>
														</div>
														<span class="requirment__slide--count">09</span>
														<h3 class="requirment__slide--title">Software Engineering</h3>
													</div>
												</div>
												<div class="swiper-slide">
													<div class="nav-link requirment__slide--link">
														<div class="iconTabs__item--image">
															<span class="tab-icon">
																<img src="src/images/icons/howJoin/Icons-Zain Women In Tech_Statistics.png" alt="" class="img-fluid"/>
															</span>
														</div>
														<span class="requirment__slide--count">10</span>
														<h3 class="requirment__slide--title">Statistics</h3>
													</div>
												</div>
											</div>
										</div>
										
									</div>
								</div>
								<div class="tab-pane fade innerTab" id="applicant" role="tabpanel" aria-labelledby="pills-applicant-tab">
									<div class="applicant__requirment mustbe">
										<div class="sliderNavigation__custom">
											<div class="sliderNavigation navigationSimple d-none">
												<div class="container">
													<div class="sliderNavigation__controls">
														<button type="button" role="button" class="sliderNavigation__controls--arrow prevArrow">
															<svg fill="currentColor">
															<g id="left-arrow">
																<path
																	fill-rule="evenodd"
																	d="M0.809,12.572 L20.175,0.476 C20.483,0.287 20.851,0.400 21.066,0.759 C21.282,1.117 21.289,1.628 21.084,1.997 L14.715,13.488 L21.084,24.978 C21.289,25.347 21.282,25.860 21.067,26.216 C20.923,26.460 20.707,26.591 20.487,26.591 C20.382,26.591 20.276,26.561 20.176,26.498 L0.811,14.403 C0.546,14.237 0.377,13.881 0.377,13.488 C0.377,13.094 0.546,12.738 0.809,12.572 Z"
																/>
															</g>
															</svg>
														</button>
														<button type="button" role="button" class="sliderNavigation__controls--arrow nextArrow">
															<svg fill="currentColor">
															<g id="right-arrow">
																<path
																	fill-rule="evenodd"
																	d="M21.190,12.572 L1.825,0.476 C1.517,0.287 1.149,0.400 0.934,0.759 C0.718,1.117 0.711,1.628 0.916,1.997 L7.285,13.488 L0.916,24.978 C0.711,25.347 0.718,25.860 0.933,26.216 C1.077,26.460 1.293,26.591 1.512,26.591 C1.618,26.591 1.724,26.561 1.824,26.498 L21.189,14.403 C21.454,14.237 21.623,13.881 21.623,13.488 C21.623,13.094 21.454,12.738 21.190,12.572 Z"
																/>
															</g>
															</svg>
														</button>
													</div>
												</div>
											</div>
										</div>
										<div class="swiper requirment"> 
											<div class="swiper-wrapper"> 
												<div class="swiper-slide">
													<div class="nav-link requirment__slide--link">
														<div class="iconTabs__item--image">
															<span class="tab-icon">
																<img src="src/images/icons/howJoin/Icons-Zain Women In Tech_Achievment_Driven.png" alt="" class="img-fluid"/>
															</span>
														</div>
														<span class="requirment__slide--count">01</span>
														<h3 class="requirment__slide--title">Achievement Driven</h3>
													</div>
												</div>
												<div class="swiper-slide">
													<div class="nav-link requirment__slide--link">
														<div class="iconTabs__item--image">
															<span class="tab-icon">
																<img src="src/images/icons/howJoin/Icons-Zain Women In Tech_Tewamwork&Collaboration.png" alt="" class="img-fluid"/>
															</span>
														</div>
														<span class="requirment__slide--count">02</span>
														<h3 class="requirment__slide--title">Teamwork and Collaboration</h3>
													</div>
												</div>
												<div class="swiper-slide">
													<div class="nav-link requirment__slide--link">
														<div class="iconTabs__item--image">
															<span class="tab-icon">
																<img src="src/images/icons/howJoin/Icons-Zain Women In Tech_Problem_Solving.png" alt="" class="img-fluid"/>
															</span>
														</div>
														<span class="requirment__slide--count">03</span>
														<h3 class="requirment__slide--title">Problem Solving</h3>
													</div>
												</div>
												<div class="swiper-slide">
													<div class="nav-link requirment__slide--link">
														<div class="iconTabs__item--image">
															<span class="tab-icon">
																<img src="src/images/icons/howJoin/Icons-Zain Women In Tech_Self_Management.png" alt="" class="img-fluid"/>
															</span>
														</div>
														<span class="requirment__slide--count">04</span>
														<h3 class="requirment__slide--title">Self-Management</h3>
													</div>
												</div>
												<div class="swiper-slide">
													<div class="nav-link requirment__slide--link">
														<div class="iconTabs__item--image">
															<span class="tab-icon">
																<img src="src/images/icons/howJoin/Icons-Zain Women In Tech_Innovation&Creativity.png" alt="" class="img-fluid"/>
															</span>
														</div>
														<span class="requirment__slide--count">05</span>
														<h3 class="requirment__slide--title">Innovation and Creativity</h3>
													</div>
												</div>
											</div>
										</div>
										
									</div>
								</div>
							</div>
						</div>
						<div class="applyBtn text-center mt-40">
							<a download href="./src/pdf/Mentee-Recruitment-WiT-Submission-Form.docx" class="btn btn-primary">Click here to download the application form to be submitted</a>
						</div>
					</div>
				</div>
			</section>
			<section data-color-theme="dark" id="faq" class="section-6">
				<div class="faq" role="region" aria-label="faqs">
					<div class="container">
						<h2>FAQs</h2>
						<div id="accordion" class="accordion">
							<div class="card">
								<div class="card-header">
									<a class="btn" data-toggle="collapse" href="#detail">What is the Application Process?</a>
								</div>
								<div id="detail" class="collapse show" data-parent="#accordion">
									<div class="card-body">
										<p class="card-text">
											<ul>
												<li>
													Submit Application Form (includes CV)
												</li>  
												<li>
													Virtual Interview with WiT team
												</li>  
											</ul>
										</p>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header">
									<a class="btn collapsed" data-toggle="collapse" href="#detail-1">Who can join?</a>
								</div>
								<div id="detail-1" class="collapse" data-parent="#accordion">
									<div class="card-body">
										<p class="card-text">											
											<p>Women who are currently in university pursuing a degree in Science, Technology, Engineering, Math (STEM) and are planning to join the workforce.</p>
										</p>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header">
									<a class="btn collapsed" data-toggle="collapse" href="#detail-2">Are there any fees or costs?</a>
								</div>
								<div id="detail-2" class="collapse" data-parent="#accordion">
									<div class="card-body">
										<p class="card-text">
											<p>No this program does not have fees.</p>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section data-color-theme="dark" id="contact" class="section-7">
				<div class="contact" aria-label="contact">
					<div class="container">
						<h2>contact us</h2>
						<div class="contact--wrapper">
							<div class="ZainWorld" id="worldofzain">
								<div class="ZainWorld__slide customTabs">
									<div class="ZainWorld__slide--head customTabs__items">
										<div class="swiper"> 
											<div class="swiper-wrapper">
												<div class="swiper-slide ZainWorld__slide--link">
													<a href="mailto:Womenintech@zain.com" class="nav-link" aria-controls="" id="" >
														<span class="icon">
															<img src="src/images/country/group.png" alt="" class="img-fluid"/>
														</span>
														General Inquiries
													</a>
												</div>
												<div class="swiper-slide ZainWorld__slide--link">
													<a href="mailto:Womenintech@kw.zain.com" class="nav-link" aria-controls="" id="" >
														<span class="icon">
															<img src="src/images/country/kuwait.png" alt="" class="img-fluid"/>
														</span>
														kuwait
													</a>
												</div>
												<div class="swiper-slide ZainWorld__slide--link">
													<a href="mailto:Womenintech@jo.zain.com" class="nav-link" aria-controls="" id="" >
														<span class="icon">
															<img src="src/images/country/jordan.png" alt="" class="img-fluid"/>
														</span>
														jordan
													</a>
												</div>
												<div class="swiper-slide ZainWorld__slide--link">
													<a href="mailto:Womenintech@sa.zain.com" class="nav-link" aria-controls="" id="" >
														<span class="icon">
															<img src="src/images/country/ksa.png" alt="" class="img-fluid"/>
														</span>
														SAUDI ARABIA
													</a>
												</div>
												<div class="swiper-slide ZainWorld__slide--link">
													<a href="mailto:Womenintech@ss.zain.com" class="nav-link" aria-controls="" id="" >
														<span class="icon">
															<img src="src/images/country/south-sudan.png" alt="" class="img-fluid"/>
														</span>
														SOUTH SUDAN
													</a>
												</div>
												<div class="swiper-slide ZainWorld__slide--link">
													<a href="mailto:Womenintech@sd.zain.com" class="nav-link" aria-controls="" id="" >
														<span class="icon">
															<img src="src/images/country/sudan.png" alt="" class="img-fluid"/>
														</span>
														SUDAN
													</a>
												</div>
												<div class="swiper-slide ZainWorld__slide--link">
													<a href="mailto:Womenintech@iq.zain.com" class="nav-link" aria-controls="" id="" >
														<span class="icon">
															<img src="src/images/country/iraq.png" alt="" class="img-fluid"/>
														</span>
														Iraq
													</a>
												</div>
												<div class="swiper-slide ZainWorld__slide--link">
													<a href="mailto:Womenintech@bh.zain.com" class="nav-link" aria-controls="" id="" >
														<span class="icon">
															<img src="src/images/country/bahrain.png" alt="" class="img-fluid"/>
														</span>
														bahrain
													</a>
												</div> 
											</div>
										</div>
										<div class="sliderNavigation side-arrows">
											<div class="container">
												<div class="sliderNavigation__control">
													<button type="button" role="button" class="sliderNavigation__control--arrow prevArrow">
														<svg>
															<g id="left-arrow">
																<path
																	fill-rule="evenodd"
																	d="M0.809,12.572 L20.175,0.476 C20.483,0.287 20.851,0.400 21.066,0.759 C21.282,1.117 21.289,1.628 21.084,1.997 L14.715,13.488 L21.084,24.978 C21.289,25.347 21.282,25.860 21.067,26.216 C20.923,26.460 20.707,26.591 20.487,26.591 C20.382,26.591 20.276,26.561 20.176,26.498 L0.811,14.403 C0.546,14.237 0.377,13.881 0.377,13.488 C0.377,13.094 0.546,12.738 0.809,12.572 Z"
																/>
															</g>
														</svg>
													</button>
													<button type="button" role="button" class="sliderNavigation__control--arrow nextArrow">
														<svg>
															<g id="right-arrow">
																<path
																	fill-rule="evenodd"
																	d="M21.190,12.572 L1.825,0.476 C1.517,0.287 1.149,0.400 0.934,0.759 C0.718,1.117 0.711,1.628 0.916,1.997 L7.285,13.488 L0.916,24.978 C0.711,25.347 0.718,25.860 0.933,26.216 C1.077,26.460 1.293,26.591 1.512,26.591 C1.618,26.591 1.724,26.561 1.824,26.498 L21.189,14.403 C21.454,14.237 21.623,13.881 21.623,13.488 C21.623,13.094 21.454,12.738 21.190,12.572 Z"
																/>
															</g>
														</svg>
													</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
    </div>
<?php include_once "includes/footer.php";?>