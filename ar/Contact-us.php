<?php $ishome = 1; include_once "includes/header.php";?>
    <div class="contactUs">
		<section id="introduction" data-color-theme="light">
			<div class="mainBanner" role="region" aria-label="introduction">
				<div class=" banner__slide" role="group">
					<div class="banner__slide--image">
						<img
							src="src/images/banners/new_contactUs__banner.png"
							alt="contact us banner"
						/>
					</div>
					<div class="banner__slide--caption">
						<div class="container">
							<h1>Contact US</h1>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s.</p>
						</div>
					</div>
				</div>
				<a
					href="#"
					class="scrollDown"
					role="button"
					aria-label="Scroll to Contents"
				>
				<span></span>
				</a>
			</div>	 
		</section>
		<div class="contentWrapper" role="region" aria-label="Contact Us" id="contactUs" data-color-theme="dark">
            <div class="container">
				<div class="contactUs__form" > 
					<form role="contact Form"> 
						<fieldset>
							<legend class="visuallyhidden">&nbsp;</legend>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">الاسم بالكامل<span>*</span></label>
										<input type="text" class="form-control" aria-label="" placeholder="الاسم بالكامل.."/>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">البريد الإلكتروني<span>*</span></label>
										<input type="email" class="form-control" aria-label="" placeholder="البريد الإلكتروني.."/>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">رقم الهاتف<span>*</span></label>
										<input type="tell" class="form-control number" aria-label="" placeholder="..رقم الاتصال"/>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">بلد الإقامة<span>*</span></label>
										<select class="selectpicker" title="بلد الإقامة">
											<option>All</option>
											<option>This is First option</option>
											<option>Selected Option</option>
											<option>Third Option</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">الجنسية<span>*</span></label>
										<select class="selectpicker" title="الجنسية">
											<option>All</option>
											<option>This is First option</option>
											<option>Selected Option</option>
											<option>Third Option</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">العنوان<span>*</span></label>
										<input type="text" class="form-control" aria-label="" placeholder="العنوان"/>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">الجامعة<span>*</span></label>
										<input type="text" class="form-control" aria-label="" placeholder="الجامعة"/>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">التخصص<span>*</span></label>
										<input type="text" class="form-control" aria-label="" placeholder="التخصص"/>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">العام المتوقع للتخرج<span>*</span></label>
										<select class="selectpicker" title="العام المتوقع للتخرج">
											<option>All</option>
											<option>This is First option</option>
											<option>Selected Option</option>
											<option>Third Option</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">المعدل التراكمي الحالي<span>*</span></label>
										<input type="text" class="form-control" aria-label="" placeholder="المعدل التراكمي الحالي"/>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label">يرجى ذكر الوظائف أو المجالات أو الصناعات التي تهتم بمعرفة المزيد عنها<span>*</span></label>
										<input type="text" class="form-control" aria-label="" placeholder="يرجى ذكر الوظائف أو المجالات أو الصناعات التي تهتم بمعرفة المزيد عنها"/>
									</div>
								</div> 
							</div>
							<div class="row">
								<div class="col-md-8">
									<div class="checkboxWrapper">
										<div class="form-group">
											<label class="control-label">هل أنت مهتم بالمساعدة في (يرجى تحديد أكبر عدد ممكن)</label>
											<ul>
												<li>
													<div class="custom-control custom-control-inline custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="checkbox-1">
														<label class="custom-control-label" for="checkbox-1">بناء شبكتك</label>
													</div>
												</li>
												<li>
													<div class="custom-control custom-control-inline custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="checkbox-2">
														<label class="custom-control-label" for="checkbox-2">تحديد الأهداف المهنية</label>
													</div>
												</li>
												<li>
													<div class="custom-control custom-control-inline custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="checkbox-3">
														<label class="custom-control-label" for="checkbox-3">خلق فرص عمل</label>
													</div>
												</li>
												<li>
													<div class="custom-control custom-control-inline custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="checkbox-4">
														<label class="custom-control-label" for="checkbox-4">التنمية الشخصية</label>
													</div>
												</li>
												<li>
													<div class="custom-control custom-control-inline custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="checkbox-5">
														<label class="custom-control-label" for="checkbox-5">تحديد أهداف الحياة</label>
													</div>
												</li>
												<li>
													<div class="custom-control custom-control-inline custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="checkbox-6">
														<label class="custom-control-label" for="checkbox-6">موازنة متطلبات الحياة</label>
													</div>
												</li>
												<li>
													<div class="custom-control custom-control-inline custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="checkbox-7">
														<label class="custom-control-label" for="checkbox-7">توسيع معرفتك بالمسارات الوظيفية في مجالات العلوم والتكنولوجيا والهندسة والرياضيات (STEM)</label>
													</div>
												</li>
												<li>
													<div class="custom-control custom-control-inline custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="checkbox-8">
														<label class="custom-control-label" for="checkbox-8">وضع أهداف / خطة تعليمية</label>
													</div>
												</li>
												<li>
													<div class="custom-control custom-control-inline custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="checkbox-9">
														<label class="custom-control-label" for="checkbox-9">وضع أهداف / خطة مهنية</label>
													</div>
												</li>
												<li>
													<div class="custom-control custom-control-inline custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="checkbox-10">
														<label class="custom-control-label" for="checkbox-10">التقدم في مجال عملك الحالي</label>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">إرفاق وتضمين السيرة الذاتية</label>
										<div class="fileUpload">
											<div class="custom-file">
												<input type="file" class="custom-file-input">
												<label class="custom-file-label">
													<span>No file chosen</span>
												</label>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label pl-0 pr-0">ملاحظة:</label>
										<p>يجب أن يكون جميع المتدربين المحتملين من النساء اللائي يدرسن حاليًا ومسجلات في جامعة تخصص في درجة متعلقة بالعلوم والتكنولوجيا والهندسة والرياضيات.</p>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="g-recaptcha brochure__form__captcha" data-sitekey="YOUR SITE KEY"></div>
							</div>
							<div class="form-group">
								<div class="button-row">
									<button type="submit" class="btn btn-primary" aria-describedby="contactform">
									إرسال
									</button>
								</div>
							</div>
						</fieldset>
					</form>
				</div> 
            </div>
        </div>		
    </div>
<?php include_once "includes/footer.php";?>