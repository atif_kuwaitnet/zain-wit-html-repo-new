var chart;
$(document).ready(function() {
    zainwit.init({});
});

var self;
var zainwit = {
    init: function(options) {
        this.settings = options;
        self = this;

        this.utilities();
        this.loader();
        this.configureModal();
        this.stickyHeader();
        this.selectpicker();
        this.sliderFunction();
        this.activeTab();
        this.setNavigaion();
    },
    loader: function() {
        setTimeout(function() {
            $('body').addClass('loaded');
        }, 2000);
    },

    stickyHeader: function() {
        $(window).scroll(function() {
            if ($(this).scrollTop() > 150) {
                $("header").addClass("sticky");

            } else {
                $("header").removeClass("sticky");

            }
        });
    },
    // selectpicker
    selectpicker: function() {
        $.fn.selectpicker.Constructor.BootstrapVersion = '4';
        if (/Android|webOS|iPhone|BlackBerry/i.test(navigator.userAgent)) {
            $.fn.selectpicker.Constructor.DEFAULTS.mobile = true;
        }
        $('select').selectpicker({

            size: 8,
            liveSearchPlaceholder: 'Search'
        });
    },
    // modal
    configureModal: function() {
        $("body").on("click", "*[data-toggle='custom-modal']", function(e) {
            e.preventDefault();
            $(".custom-modal").removeClass("large");
            var url = $(this).attr("data-path");
            var size = $(this).attr("data-size");
            var class_name = $(this).attr("data-class");
            $(".custom-modal").removeClass("large");
            $(".custom-modal").removeClass("medium");
            $(".custom-modal").removeClass("small");
            $.get(url, function(data) {
                $(".custom-modal").modal("show");
                $(".custom-modal .modal-body").html(data);

                if (size) {
                    $(".custom-modal").addClass(size);
                }
                if (class_name) {
                    $(".custom-modal").addClass(class_name);
                }
                setTimeout(function() {
                    $(".custom-modal .modal-body").addClass("show");
                }, 200);
                $("body").addClass("remove-scroll");
            });
        });
        $(".modal").on("hidden.bs.modal", function() {
            $(".custom-modal .modal-body").removeClass("show");
            $(".custom-modal .modal-body").empty();
            $(".custom-modal").removeClass("account-modal");
            $("body").removeClass("remove-scroll");
            $(".custom-modal").removeClass("large");
            $(".custom-modal").removeClass("medium");
            $(".custom-modal").removeClass("small");
        });
    },
    utilities: function () {

        // setTimeout(() => {
        //     document.querySelector('.modal.show .popup-cross-icon').focus();
        // }, 2000);

        $(".modal.show").find(".popup-cross-icon").focus();

        // mCustomScrollbar
        $(".inner-scroll").mCustomScrollbar();

        $(".scrollDown").on("click", function (e) {
            e.preventDefault();
            let headerHeight = document.querySelector('.mainBanner').offsetHeight - 40;
            document.body.scrollTop = headerHeight
            document.documentElement.scrollTop = headerHeight
        });

         $(".discover").on("click", function (e) {
            e.preventDefault();
            let headerHeight = document.querySelector('.mainBanner').offsetHeight - 40;
            document.body.scrollTop = headerHeight
            document.documentElement.scrollTop = headerHeight
        });

        // side menu sticky 
        // meun sticky //

        $(".pageSections .navbar li").click(function () {
            $(this).addClass("active").siblings().removeClass("active");
        });

        $(".pageSections .navbar li").click(function () {
            $("html, body").animate({
                scrollTop: $("#" + $(this).data("value")).offset().top,
            },
                800
            );
        });

        $(window).scroll(function () {
            $("section").each(function () {
                if ($(this).hasClass("section-1")) {
                    if ($(window).scrollTop() >= $(this).offset().top - 265) {
                        $(".pageSections .navbar li:first-of-type()")
                            .addClass("active ")
                            .siblings()
                            .removeClass("active ");
                    }
                }
                else if ($(this).hasClass("section-2")) {
                    if ($(window).scrollTop() >= $(this).offset().top - 265) {
                        $(".pageSections .navbar li:nth-of-type(2)")
                            .addClass("active")
                            .siblings()
                            .removeClass("active");

                        $(".pageSections .navbar li:nth-of-type(2) a").addClass("activeColor");
                    } 
                }
                else if ($(this).hasClass("section-3")) {
                    if ($(window).scrollTop() >= $(this).offset().top - 265) {
                        $(".pageSections .navbar li:nth-of-type(3)")
                            .addClass("active")
                            .siblings()
                            .removeClass("active ");
                    }
                }
                else if ($(this).hasClass("section-4")) {
                    if ($(window).scrollTop() >= $(this).offset().top - 265) {
                        $(".pageSections .navbar li:nth-of-type(4)")
                            .addClass("active")
                            .siblings()
                            .removeClass("active");

                    }
                }
                else if ($(this).hasClass("section-5")) {
                    if ($(window).scrollTop() >= $(this).offset().top - 265) {
                        $(".pageSections .navbar li:nth-of-type(5)")
                            .addClass("active")
                            .siblings()
                            .removeClass("active");

                        $(".pageSections .navbar li:nth-of-type(5) a").addClass("activeColor");
                    }
                }
                else if ($(this).hasClass("section-6")) {
                    if ($(window).scrollTop() >= $(this).offset().top - 350) {
                        $(".pageSections .navbar li:nth-of-type(6)")
                            .addClass("active")
                            .siblings()
                            .removeClass("active");

                        $(".pageSections .navbar li:nth-of-type(6) a").addClass("activeColor");
                    }
                }
                else if ($(this).hasClass("section-7")) {
                    if ($(window).scrollTop() >= $(this).offset().top - 350) {
                        $(".pageSections .navbar li:nth-of-type(7)")
                            .addClass("active")
                            .siblings()
                            .removeClass("active");
                        $(".pageSections .navbar li:nth-of-type(7) a").addClass("activeColor");
                    }
                }
                else if ($(this).hasClass("section-8")) {
                    if ($(window).scrollTop() >= $(this).offset().top - 400) {
                        $(".pageSections .navbar li:nth-of-type(8)")
                            .addClass("active")
                            .siblings()
                            .removeClass("active");
                    }
                }

                // our impact 
                else if ($(this).hasClass("impact-sec2")) {
                    if ($(window).scrollTop() >= $(this).offset().top - 0) {
                        $(".pageSections.ourImpect-section .navbar li:nth-of-type(2)")
                            .addClass("active")
                            .siblings()
                            .removeClass("active");
                        $(".pageSections.ourImpect-section .navbar li:nth-of-type(2) a").addClass("activeColor");
                    }
                }
                
                // our impact 
                
            });
            if (window.innerHeight + window.scrollY + 2 >= document.body.offsetHeight) {
                $(".pageSections .navbar").addClass("activeTop");
            } else {
                $(".pageSections .navbar").removeClass("activeTop");
            }
        });
        // side menu sticky 
    },
    // slider functions
    sliderFunction: function() {
        let meetMentors = new Swiper('.meetMentorsMain .swiper', {
            slidesPerView: 1,
            spaceBetween: 0,
            speed: 1000,
            navigation: {
                "arrows": true,
                "infinite": true,
                "prevEl": ".meetMentorsMain .prevArrow",
                "nextEl": ".meetMentorsMain .nextArrow"
            },
            breakpoints: {
                1200: {
                    slidesPerView: 7,
                },
                768: {
                    slidesPerView: 5,
                },
                680: {
                    slidesPerView: 4,
                },
                576: {
                    slidesPerView: 3,
                },
                340: {
                    slidesPerView: 2,
                },
            },
        });
        let program = new Swiper('.program__head .swiper', {
            slidesPerView: 2,
            spaceBetween: 0,
            observer: true,
            observeParents: true,
            slideToClickedSlide: true,
            navigation: {
                "arrows": true,
                "infinite": true,
                "prevEl": ".program__head .prevArrow",
                "nextEl": ".program__head .nextArrow"
            },
            breakpoints: {
                576: {
                    slidesPerView: 5,
                },
                415: {
                    slidesPerView: 3,
                },
            },
        });
        let responibility_Meters = new Swiper('.responsibility__content .swiper', {
            slidesPerView: 1,
            spaceBetween: 0,
            observer: true,
            observeParents: true,
            slideToClickedSlide: true,
            navigation: {
                "arrows": true,
                "infinite": true,
                "prevEl": ".responsibility__content .prevArrow",
                "nextEl": ".responsibility__content .nextArrow"
            },
            breakpoints: {
                1301: {
                    slidesPerView: 8,
                },
                1201: {
                    slidesPerView: 7,
                },
                1025: {
                    slidesPerView: 5,
                },
                768: {
                    slidesPerView: 4,
                },
                576: {
                    slidesPerView: 3,
                },
                450: {
                    slidesPerView: 2,
                },
            },
        });
        let zainWorld = new Swiper('.ZainWorld__slide .swiper', {
            slidesPerView: 1,
            spaceBetween: 0,
            observer: true,
            observeParents: true,
            navigation: {
                "arrows": true,
                "infinite": true,
                "prevEl": ".ZainWorld__slide .prevArrow",
                "nextEl": ".ZainWorld__slide .nextArrow"
            },
            breakpoints: { 
                1201: {
                    slidesPerView: 7,
                },
                992: {
                    slidesPerView: 5,
                },
                768: {
                    slidesPerView: 4,
                },
                576: {
                    slidesPerView: 3,
                },
                415: {
                    slidesPerView: 2,
                },
            },
        });
        let countries = new Swiper('.ZainWorld__slide .swiper', {
            slidesPerView: 1,
            spaceBetween: 0,
            observer: true,
            observeParents: true,
            navigation: {
                "arrows": true,
                "infinite": true,
                "prevEl": ".ZainWorld__slide .prevArrow",
                "nextEl": ".ZainWorld__slide .nextArrow"
            },
            breakpoints: {
                1201: {
                    slidesPerView: 8,
                },
                992: {
                    slidesPerView: 6,
                },
                768: {
                    slidesPerView: 4,
                },
                576: {
                    slidesPerView: 3,
                },
                415: {
                    slidesPerView: 2,
                },
            },
        });
        let join = new Swiper('.applicant__requirment.required .swiper', {
            slidesPerView: 1,
            spaceBetween: 0,
            // observer: true,
            // observeParents: true,
            // slideToClickedSlide: true,
            navigation: {
                "arrows": true,
                "infinite": true,
                "prevEl": ".applicant__requirment.required .prevArrow",
                "nextEl": ".applicant__requirment.required .nextArrow"
            },
            breakpoints: {
                992: {
                    slidesPerView: 5,
                },
                651: {
                    slidesPerView: 4,
                },
                451: {
                    slidesPerView: 3,
                },
                351: {
                    slidesPerView: 2,
                },
            },
        });
        let joinMust = new Swiper('.applicant__requirment.mustbe .swiper', {
            slidesPerView: 1,
            spaceBetween: 0,
            // observer: true,
            // observeParents: true,
            // slideToClickedSlide: true,
            navigation: {
                "arrows": true,
                "infinite": true,
                "prevEl": ".applicant__requirment.mustbe .prevArrow",
                "nextEl": ".applicant__requirment.mustbe .nextArrow"
            },
            breakpoints: {
                992: {
                    slidesPerView: 5,
                },
                651: {
                    slidesPerView: 4,
                },
                451: {
                    slidesPerView: 3,
                },
                351: {
                    slidesPerView: 2,
                },
            },
        });
        let our_purpose = new Swiper('.about__slider .swiper', {
            autoplay: {
                disableOnInteraction: false,
            },
            effect: 'coverflow',
            speed: 800,
            loop: false,
        });
        let mediaSliderMain = new Swiper('.mediaSlider__slides .swiper', {
            slidesPerView: 1,
            spaceBetween: 0,
            speed:1500,
            navigation: {
                "arrows": true,
                "slidesToShow": 1,
                "infinite": true,
                "prevEl": ".mediaSlider__slides .prevArrow",
                "nextEl": ".mediaSlider__slides .nextArrow"
            },
            pagination: {
                el: '.swiper-pagination',
                type: 'fraction',
            },
        });
        let  playIcon = document.querySelector('.sliderNavigation__controls--icon .slider-play');
        let  pauseIcon = document.querySelector('.sliderNavigation__controls--icon .slider-pause');
        if(playIcon){
                playIcon.addEventListener('click',()=>{
                mediaSliderMain.autoplay.start();
                playIcon.style.display='none';
                pauseIcon.style.display='block';
            });
        }
       if(pauseIcon){
            pauseIcon.addEventListener('click',()=>{
            mediaSliderMain.autoplay.stop();
            playIcon.style.display='block';
            pauseIcon.style.display='none';
        });
       }
       

        // let testimonials = new Swiper('.testimonials__slider .swiper', {
        //     slidesPerView: 1,
        //     spaceBetween: 0,
        //     speed:1500,
        //     navigation: {
        //         "arrows": true,
        //         "slidesToShow": 1,
        //         "infinite": true,
        //         "prevEl": ".testimonials__slider .prevArrow",
        //         "nextEl": ".testimonials__slider .nextArrow"
        //     },
        //     pagination: {
        //         el: '.swiper-pagination',
        //         type: 'fraction',
        //     },
        // });
    },
    setNavigaion: function() {
        
    },
    activeTab: function() {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(event) {
            $(".swiper .nav-link.active").removeClass("active");
            $(this).addClass("active");
        });

        let totalSlide = document.querySelectorAll('.customTabs__items .swiper-slide');
        let arrows = document.querySelectorAll('.sliderNavigation__controls button');
        let tabs = document.querySelectorAll('.tab-pane:not(.innerTab)');

        arrows.forEach(arrow => {
            arrow.addEventListener('click', () => {
                tabs.forEach(i => i.classList.remove("active", "show"))
                totalSlide.forEach(slides => {

                    slides.querySelector('.nav-link').classList.remove('active')

                    if (slides.classList.contains('swiper-slide-active')) {
                        slides.querySelector('.nav-link').classList.add('active');
                        let currentSlide = slides.querySelector('a').getAttribute('href');
                        let replacehref = currentSlide.replace('#', '')
                        tabs.forEach(i => {
                            let currentTab = i.getAttribute('id');
                            if (currentTab.includes(replacehref)) {
                                i.classList.add('active', 'show')
                            } else {
                                i.classList.remove('active', 'show')
                            }
                        })
                    } else {
                        // slides.querySelector('.nav-link').classList.remove('active')
                    }
                })
            })
        })
    },
};
var lazyload = {
    load: function(wrapper, dataURL) {
        $(".marker-end")
            .on('lazyshow', function() {
                if ($("#loadmorecount").val() < 3) {
                    $.ajax({
                        url: dataURL,
                        dataType: "html",
                        success: function(responseText) {
                            setTimeout(function() {
                                if (responseText != "") {
                                    $(wrapper).append($.parseHTML(responseText));
                                    $(window).lazyLoadXT();
                                    $('.marker-end').lazyLoadXT({ visibleOnly: false, checkDuplicates: false });
                                } else {
                                    $('.marker-end').hide();
                                }
                            }, 700);

                        },
                        complete: function() {
                            $("#loadmorecount").val(parseInt($("#loadmorecount").val()) + 1);
                        }
                    })
                } else {
                    $('.marker-end').hide();
                }
            })
            .lazyLoadXT({ visibleOnly: false });
    }
};