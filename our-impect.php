<?php $ishome = 1; include_once "includes/header.php";?>
<!-- page section -->
	<div class="pageSections ourImpect-section" data-theme="light">
		<nav  class="navbar bg-faded navbar-light fixed-top navbar-expand" aria-label="Page Navigation">
			<ul  class="nav nav-pills">
				<li class="nav-item active">
					<a  href="#header" target="_self" class="nav-link active">
						<span>Header</span> 
					</a>
				</li>
				<li class="nav-item">
					<a href="#targets_measurements" data-theme='dark' target="_self" class="nav-link" >
						<span>targets and measurement</span>
					</a>
				</li>
				<li class="nav-item">
					<a href="#footer" target="_self" class="nav-link">
						<span>Footer</span>
					</a>
				</li>
			</ul>
		</nav>
	</div>
<!-- page section -->

	<div class="ourImpect">
		<section>
			<div class="mainBanner" role="region" aria-label="mainBanner"> 
				<div class=" banner__slide" role="group">
					<div class="banner__slide--image">
						<img
							src="src/images/banners/impectbanner.png"
							alt="main banner image"
						/>
					</div>
					<div class="banner__slide--caption">
						<div class="container">
							<h1>our impact</h1>
						</div>
					</div>
				</div>
				<a
					href="#"
					class="scrollDown"
					role="button"
					aria-label="Scroll to Contents"
				>
				<span></span>
				</a>
			</div>
		</section>
		<div class="contentWrapper">
		<section data-color-theme="dark" id="targets_measurements" class="impact-sec2">
				<div class="targetMeasurement" role="region" aria-label="ariaLabel">
					<div class="container">
						<div class="grid" data-grid-item-width="1/2" data-grid-item-gap="40">
							<div class="targetMeasurement__contents">
								<h2 id="zainpioneer"  aria-label="Targets and Measurements">Targets and Measurements</h2>
								<p>
									The pilot phase of the program starting (August 2021 – December 2021) will be selecting 5 mentees from each operation to join the program. We will be conducting focus group and feedback sessions with all stakeholders involved to gain better insights and set accurate targets.
								</p>
							</div>
							<div class="targetMeasurement__image">
								<img src="src/images/targetimg1.jpg" class="img-fluid " alt="targetimg1"/>
								<img src="src/images/targetimg2.jpg" class="img-fluid img-top" alt="targetimg2"/>
							</div>
						</div>
					</div>
				</div>
		</section>
		<section>
			<div class="ourImpect" aria-label="ariaLabel">
				<div class="container">
				<h2 class="sectionTitle" aria-label="We will be measuring/tracking the below for">We will be measuring/tracking the below for</h2>
					<div class="tabs">
						<ul class="nav nav-pills" id="pills-tab" role="tablist">
							<li class="nav-item" role="presentation">
								<a class="nav-link active" data-toggle="pill" href="#Mentees" role="tab" aria-controls="pills-Mentees" aria-selected="true">Mentees</a>
							</li>
							<li class="nav-item" role="presentation">
								<a class="nav-link" data-toggle="pill" href="#Mentors" role="tab" aria-controls="pills-Mentors" aria-selected="false">Mentors</a>
							</li>
						</ul>
						<div class="tab-content mt-20">
							<div class="tab-pane fade show active" id="Mentees" role="tabpanel" aria-labelledby="pills-Mentees-tab">
								<ul>
									<li ><p>Skill, attitude, knowledge & behavior development</p> </li>
									<li ><p>Internships, certifications & networking opportunities attained</p> </li>
									<li ><p>Increase in motivation to pursue STEM career</p> </li>
									<li ><p>Improvement in academic performance</p> </li>
								</ul>
							</div>
							<div class="tab-pane fade" id="Mentors" role="tabpanel" aria-labelledby="pills-Mentors-tab">
								<ul>
									<li ><p>Track career advancement for mentors</p> </li>
									<li ><p>Mentee and mentor satisfaction with mentorship relationship</p> </li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- <section data-color-theme="dark" id="testimonials">
			<div class="testimonials" role="region" aria-label="testimonials">
				<div class="container">
					<h2>testimonials</h2>
					<div class="testimonials__slider">
						<div class="swiper">
							<div class="swiper-wrapper" role="tablist">
								<div class="swiper-slide" role="presentation">
									<div class="row align-items-center">
										<div class="col-md-3">
											<div class="image">
												<img src="src/images/ourImpect/testimonials1.jpg" class="img-fluid" alt="">
											</div>
										</div>
										<div class="col-md-9">
											<div class="desc">
												<p class="intro">It's a place of a good knowledge, where you can transform this knowledge into an impact, where you meet great people and mentors, but above all it made my dream come true.</p>
												<div>
													<p class="name">Abdulla abu karaki</p>
													<p class="designation">Idea owner</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="swiper-slide" role="presentation">
									<div class="row align-items-center">
										<div class="col-md-3">
											<div class="image">
												<img src="src/images/ourImpect/testimonials1.jpg" class="img-fluid" alt="">
											</div>
										</div>
										<div class="col-md-9">
											<div class="desc">
												<p class="intro">It's a place of a good knowledge, where you can transform this knowledge into an impact, where you meet great people and mentors, but above all it made my dream come true.</p>
												<div>
													<p class="name">Abdulla abu karaki</p>
													<p class="designation">Idea owner</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="swiper-slide" role="presentation">
									<div class="row align-items-center">
										<div class="col-md-3">
											<div class="image">
												<img src="src/images/ourImpect/testimonials1.jpg" class="img-fluid" alt="">
											</div>
										</div>
										<div class="col-md-9">
											<div class="desc">
												<p class="intro">It's a place of a good knowledge, where you can transform this knowledge into an impact, where you meet great people and mentors, but above all it made my dream come true.</p>
												<div>
													<p class="name">Abdulla abu karaki</p>
													<p class="designation">Idea owner</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="swiper-slide" role="presentation">
									<div class="row align-items-center">
										<div class="col-md-3">
											<div class="image">
												<img src="src/images/ourImpect/testimonials1.jpg" class="img-fluid" alt="">
											</div>
										</div>
										<div class="col-md-9">
											<div class="desc">
												<p class="intro">It's a place of a good knowledge, where you can transform this knowledge into an impact, where you meet great people and mentors, but above all it made my dream come true.</p>
												<div>
													<p class="name">Abdulla abu karaki</p>
													<p class="designation">Idea owner</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="swiper-slide" role="presentation">
									<div class="row align-items-center">
										<div class="col-md-3">
											<div class="image">
												<img src="src/images/ourImpect/testimonials1.jpg" class="img-fluid" alt="">
											</div>
										</div>
										<div class="col-md-9">
											<div class="desc">
												<p class="intro">It's a place of a good knowledge, where you can transform this knowledge into an impact, where you meet great people and mentors, but above all it made my dream come true.</p>
												<div>
													<p class="name">Abdulla abu karaki</p>
													<p class="designation">Idea owner</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="sliderNavigation">
							<div class="container">
								<div class="sliderNavigation__controls">
									<div class="sliderNavigation__controls--pagination">
										<div class="swiper-pagination"></div>
									</div>
									<button type="button" role="button" class="sliderNavigation__controls--arrow prevArrow">
										<svg>
											<g id="left-arrow">
												<path
													fill-rule="evenodd"
													d="M0.809,12.572 L20.175,0.476 C20.483,0.287 20.851,0.400 21.066,0.759 C21.282,1.117 21.289,1.628 21.084,1.997 L14.715,13.488 L21.084,24.978 C21.289,25.347 21.282,25.860 21.067,26.216 C20.923,26.460 20.707,26.591 20.487,26.591 C20.382,26.591 20.276,26.561 20.176,26.498 L0.811,14.403 C0.546,14.237 0.377,13.881 0.377,13.488 C0.377,13.094 0.546,12.738 0.809,12.572 Z"
												/>
											</g>
										</svg>
									</button>
									<button type="button" role="button" class="sliderNavigation__controls--arrow nextArrow">
										<svg>
											<g id="right-arrow">
												<path
													fill-rule="evenodd"
													d="M21.190,12.572 L1.825,0.476 C1.517,0.287 1.149,0.400 0.934,0.759 C0.718,1.117 0.711,1.628 0.916,1.997 L7.285,13.488 L0.916,24.978 C0.711,25.347 0.718,25.860 0.933,26.216 C1.077,26.460 1.293,26.591 1.512,26.591 C1.618,26.591 1.724,26.561 1.824,26.498 L21.189,14.403 C21.454,14.237 21.623,13.881 21.623,13.488 C21.623,13.094 21.454,12.738 21.190,12.572 Z"
												/>
											</g>
										</svg>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</section> -->
		</div>
	</div>
<?php include_once "includes/footer.php";?>