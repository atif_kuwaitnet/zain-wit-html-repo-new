<div
    class="menuWrapper"
    aria-hidden="true"
    tabindex="-1"
    
  >
    <div class="close-icon">
      <a
        aria-label="Close Main Navigation" 
        class="closeMenu"
        href="#"
        
      >
        <span class="icon">
          
            <svg fill="currentColor" viewBox="0 0 512 512" >
                <g id="cross-icon">
                    <path
                    d="M284.29,256,506.14,34.14A20,20,0,0,0,477.86,5.86L256,227.72,34.14,5.86A20,20,0,0,0,5.86,34.14L227.71,256,5.86,477.86a20,20,0,1,0,28.28,28.28L256,284.29,477.86,506.14a20,20,0,0,0,28.28-28.28Z"
                    />
                </g>
            </svg> 
        </span
        ><span class="text">Close</span>
      </a>
    </div>
    <div class="container">
      <div class="main-content">
        <nav aria-label="Main Menu">
          <ul class="mainNav" role="menubar">
            <li role="none">
              <a href="Home.php" role="menuitem">Home</a>
            </li>
            <li role="none">
              <a href="Home.php#how_join" role="menuitem">how to join?</a>
            </li>
            <li role="none">
              <a href="meet-mentors.php" role="menuitem">MEET THE MENTORS</a>
            </li>
            <li role="none">
              <a href="our-impect.php" role="menuitem">our impact</a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </div>