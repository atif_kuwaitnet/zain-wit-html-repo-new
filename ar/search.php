<?php $ishome = 1; include_once "includes/header.php";?>
    <div class="search contentWrapper" role="search">
		<div class="container">
            <div class="siteSearch">
                <h2 class="search__title">search</h2>
                <div class="search__head"> 
					<div class="search-field">
						<form role="search">
							<fieldset>
								<legend class="visuallyhidden">&nbsp;</legend>
								<div class="FormField">
									<div class="FormField__input">
										<input
											type="text"
											aria-label=""
											placeholder="عربى"
										/>
									</div>
									<div class="FormField__button">
										<button>
											<i>بحث</i>
											<span>
												<div>
													<svg fill="currentColor" >
														<g id="form-search-icon">
															<path
																d="M5.65,14a1.11,1.11,0,0,1-1.1-1A7.55,7.55,0,0,1,6.72,6.83,1.1,1.1,0,1,1,8.28,8.39a5.38,5.38,0,0,0-1.52,4.36,1.11,1.11,0,0,1-1,1.21Z"
															/>
															<path
																d="M12.67,25.34A12.67,12.67,0,1,1,25.34,12.67,12.68,12.68,0,0,1,12.67,25.34Zm0-23.12A10.46,10.46,0,1,0,23.13,12.67,10.46,10.46,0,0,0,12.67,2.22Z"
															/>
															<path
																d="M28.89,30a1.11,1.11,0,0,1-.78-.32l-8.06-8.06a1.11,1.11,0,0,1,1.57-1.57l8.06,8.06a1.12,1.12,0,0,1,0,1.57A1.14,1.14,0,0,1,28.89,30Z"
															/>
														</g>
													</svg>
												</div>									
											</span>
										</button>
									</div>
								</div>
							</fieldset>
						</form>
						<span class="search__results">
							حوالي نتيجة واحدة (0.49 ثانية)	
						</span>
					</div>
                </div>
                <div class="baseSearchlist">
					<div class="baseSearchlist__item">
						<div class="baseSearchlist__section">
							<h2>
								<a href="" class="baseSearchlist__section--title"> about us </a>
							</h2>
							<a href="" class="baseSearchlist__section--subtitle">
								<span class="icon">
									<img src="src/images/search/icons/ceo-message.png" class="img-fluid" alt="">
								</span>
							Program 
							</a>
						</div>
						<div class="baseSearchlist__card">
							<div class="row align-items-center">
								<div class="col-md-7">
									<span class="baseSearchlist__card--date">5 MAY - 2020</span>
									<h3>
										<a
											class="baseSearchlist__card--title"
											href="/"
											id="name"
										>
											Lorem ipsum dolor sit amet, consetetur sadipscing elitr
										</a>
									</h3>
									<div class="baseSearchlist__card--description">
										<p>
										Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur
										</p>
									</div>
									<button
										class="baseSearchlist__card--button btn btn-primary d-md-block d-none"
										aria-describedby="name"
									>
										read more
									</button>
								</div>
								<div class="col-md-5">
									<div class="baseSearchlist__card--media">
										<div class="media-image">
											<img
												src="src/images/search/person2.png"
												alt=""
												class="img-fluid"
											/>
											<a href="#" class="abs-link"></a>
										</div>
										<div class="text-center d-md-none d-block">
											<button
												class="baseSearchlist__card--button btn btn-primary "
												aria-describedby="name"
											>
												read more
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="baseSearchlist__item">
						<div class="baseSearchlist__section">
							<h2>
								<a href="" class="baseSearchlist__section--title">
									How to Join?
								</a>
							</h2>
							<a href="" class="baseSearchlist__section--subtitle">
								<span class="icon">
									<img src="src/images/search/icons/press-releases.png" class="img-fluid" alt="">
								</span>
								Application Requirements
							</a>
						</div>
						<div class="baseSearchlist__card">
							<div class="row align-items-center">
								<div class="col-md-7">
									<span class="baseSearchlist__card--date">5 MAY - 2020</span>
									<h3>
										<a
											class="baseSearchlist__card--title"
											href="/"
											id="name2"
										>
											Lorem ipsum dolor sit amet, consetetur sadipscing elitr
										</a>
									</h3>
									<div class="baseSearchlist__card--description">
										<p>
											Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
										</p>
									</div>
									<button
										class="baseSearchlist__card--button btn btn-primary d-md-block d-none"
										aria-describedby="name2"
									>
										read more
									</button>
								</div>
								<div class="col-md-5">
									<div class="baseSearchlist__card--media slider">
										<div class="media-image">
											<img
												src="src/images/search/press-release.png"
												alt=""
												class="img-fluid"
											/>
											<a href="#" class="abs-link"></a>
										</div>
										<div class="text-center d-md-none d-block">
											<button
												class="baseSearchlist__card--button btn btn-primary "
												aria-describedby="name"
											>
												read more
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="baseSearchlist__item">
						<div class="baseSearchlist__section">
							<h2>
								<a href="" class="baseSearchlist__section--title"> Our Imapct </a>
							</h2>
							<a href="" class="baseSearchlist__section--subtitle">
								<span class="icon">
									<img src="src/images/search/icons/video-gallery.png" class="img-fluid" alt="">
								</span>
								Testimonials
							</a>
						</div>
						<div class="baseSearchlist__card">
							<div class="row align-items-center">
								<div class="col-md-7">
									<span class="baseSearchlist__card--date">5 MAY - 2020</span>
									<h3>
										<a
											class="baseSearchlist__card--title"
											href="/"
											id="name3"
										>
										Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore
										</a>
									</h3>
									<div class="baseSearchlist__card--description">
										<p></p>
									</div>
									<button
										class="baseSearchlist__card--button btn btn-primary"
										aria-describedby="name3"
									>
										read more
									</button>
								</div>
								<div class="col-md-5">
									<div class="baseSearchlist__card--media">
										<div class="mediaVideo">
											<a class="" href="#" data-toggle="modal" data-target="#video">
												<span class="icon">
													<div>
														<svg viewBox="0 0 163.861 163.861" fill="currentColor" >
														<g id="slider-play">
															<path
																d="M34.857,3.613C20.084-4.861,8.107,2.081,8.107,19.106v125.637c0,17.042,11.977,23.975,26.75,15.509L144.67,97.275c14.778-8.477,14.778-22.211,0-30.686L34.857,3.613z"
															/>
														</g>
														</svg>
													</div>
												</span>
												<div class="mediaVideo__thumbnail">
													<img
														src="src/images/search/videoThumb2.png"
														alt=""
														class="img-fluid"
													/>
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- pagination  -->
					<div class="overflow-auto">
						<nav aria-label="Page navigation example">
							<ul class="pagination justify-content-center">
								<li class="page-item">
									<a class="page-link" href="#" aria-label="Previous">
									</a>
								</li>
								<li class="page-item active"><a class="page-link" href="#">1</a></li>
								<li class="page-item"><a class="page-link" href="#">2</a></li>
								<li class="page-item"><a class="page-link" href="#">3</a></li>
								<li class="page-item"><a class="page-link" href="#">4</a></li>
								<li class="page-item">
									<a class="page-link" href="#" aria-label="Next">
									</a>
								</li>
							</ul>
						</nav>
					</div>
				</div>
            </div>
        </div>		
    </div>

	<!-- modal -->
	<div class="modal videoModal fade" id="video" tabindex="-1" role="dialog" aria-labelledby="video" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="videoWrapper">
					<iframe src="https://www.youtube.com/embed/w8K8aj3K1OQ"></iframe>
				</div>
			</div>
			</div>
		</div>
	</div>
	<!-- End modal -->
<?php include_once "includes/footer.php";?>

<script>
	$(document).ready(function(){
	 $("body").addClass("searchPage");
	});
</script>