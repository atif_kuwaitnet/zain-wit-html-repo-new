<?php $ishome = 1; include_once "includes/header.php";?>
	<div class="meetMentorsMain">
		<section>
			<div class="mainBanner" role="region" aria-label="mainBanner"> 
				<div class=" banner__slide" role="group">
					<div class="banner__slide--image">
						<img
							src="src/images/banners/mentorsbanner.png"
							alt="main banner image"
						/>
					</div>
					<div class="banner__slide--caption">
						<div class="container">
							<h1>MEET THE MENTORS</h1>
						</div>
					</div>
				</div>
				<a
					href="#"
					class="scrollDown"
					role="button"
					aria-label="Scroll to Contents"
				>
				<span></span>
				</a>
			</div>
		</section>
		<div class="container">
			<div class="meetMentorsMain__slide">
				<div class="meetMentorsMain__slide--head customTabs__items">
					<div class="swiper">
						<div class="swiper-wrapper" role="tablist">
							<div class="swiper-slide meetMentorsMain__slide--link" role="presentation">
								<a
									class="nav-link active"
									data-toggle="tab"
									href="#kuwait"
									role="tab"
									aria-controls="tab_meetMentorsMain_0"
									aria-selected="true"
									id="meetMentorsMain_0"
								>
									<span class="icon">
										<img
											src="src/images/country/kuwait.png"
											alt=""
											class="img-fluid"
										/>
									</span>
									kuwait
								</a>
							</div>
							<div class="swiper-slide meetMentorsMain__slide--link" role="presentation">
								<a
									class="nav-link"
									data-toggle="tab"
									href="#jordan"
									role="tab"
									aria-controls="tab_meetMentorsMain_1"
									aria-selected="false"
									id="meetMentorsMain_1"
								>
									<span class="icon">
										<img
											src="src/images/country/jordan.png"
											alt=""
											class="img-fluid"
										/>
									</span>
									jordan
								</a>
							</div>
							<div class="swiper-slide meetMentorsMain__slide--link" role="presentation">
								<a
									class="nav-link"
									data-toggle="tab"
									href="#ksa"
									role="tab"
									aria-controls="tab_meetMentorsMain_2"
									aria-selected="false"
									id="meetMentorsMain_2"
								>
									<span class="icon">
										<img
											src="src/images/country/ksa.png"
											alt=""
											class="img-fluid"
										/>
									</span>
									ksa
								</a>
							</div>
							<div class="swiper-slide meetMentorsMain__slide--link" role="presentation">
								<a
									class="nav-link"
									data-toggle="tab"
									href="#iraq"
									role="tab"
									aria-controls="tab_meetMentorsMain_3"
									aria-selected="false"
									id="meetMentorsMain_3"
								>
									<span class="icon">
										<img
											src="src/images/country/iraq.png"
											alt=""
											class="img-fluid"
										/>
									</span>
									iraq
								</a>
							</div>
							<div class="swiper-slide meetMentorsMain__slide--link" role="presentation">
								<a
									class="nav-link"
									data-toggle="tab"
									href="#bahrain"
									role="tab"
									aria-controls="tab_meetMentorsMain_4"
									aria-selected="false"
									id="meetMentorsMain_4"
								>
									<span class="icon">
										<img
											src="src/images/country/bahrain.png"
											alt=""
											class="img-fluid"
										/>
									</span>
									bahrain
								</a>
							</div>
							<div class="swiper-slide meetMentorsMain__slide--link" role="presentation">
								<a
									class="nav-link"
									data-toggle="tab"
									href="#sudan"
									role="tab"
									aria-controls="tab_meetMentorsMain_5"
									aria-selected="false"
									id="meetMentorsMain_5"
								>
									<span class="icon">
										<img
											src="src/images/country/sudan.png"
											alt=""
											class="img-fluid"
										/>
									</span>
									sudan
								</a>
							</div>
							<div class="swiper-slide meetMentorsMain__slide--link" role="presentation">
								<a
									class="nav-link"
									data-toggle="tab"
									href="#southSudan"
									role="tab"
									aria-controls="tab_meetMentorsMain_6"
									aria-selected="false"
									id="meetMentorsMain_6"
								>
									<span class="icon">
										<img
											src="src/images/country/south-sudan.png"
											alt=""
											class="img-fluid"
										/>
									</span>
									south sudan
								</a>
							</div>
						</div>
					</div>
					<div class="sliderNavigation side-arrows">
						<div class="container">
							<div class="sliderNavigation__controls">
								<button type="button" role="button" class="sliderNavigation__controls--arrow prevArrow">
									<svg>
										<g id="left-arrow">
											<path
												fill-rule="evenodd"
												d="M0.809,12.572 L20.175,0.476 C20.483,0.287 20.851,0.400 21.066,0.759 C21.282,1.117 21.289,1.628 21.084,1.997 L14.715,13.488 L21.084,24.978 C21.289,25.347 21.282,25.860 21.067,26.216 C20.923,26.460 20.707,26.591 20.487,26.591 C20.382,26.591 20.276,26.561 20.176,26.498 L0.811,14.403 C0.546,14.237 0.377,13.881 0.377,13.488 C0.377,13.094 0.546,12.738 0.809,12.572 Z"
											/>
										</g>
									</svg>
								</button>
								<button type="button" role="button" class="sliderNavigation__controls--arrow nextArrow">
									<svg>
										<g id="right-arrow">
											<path
												fill-rule="evenodd"
												d="M21.190,12.572 L1.825,0.476 C1.517,0.287 1.149,0.400 0.934,0.759 C0.718,1.117 0.711,1.628 0.916,1.997 L7.285,13.488 L0.916,24.978 C0.711,25.347 0.718,25.860 0.933,26.216 C1.077,26.460 1.293,26.591 1.512,26.591 C1.618,26.591 1.724,26.561 1.824,26.498 L21.189,14.403 C21.454,14.237 21.623,13.881 21.623,13.488 C21.623,13.094 21.454,12.738 21.190,12.572 Z"
											/>
										</g>
									</svg>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="meetMentorsMain__detail">
				<div class="tab-content">
					<div class="tab-pane fade show active" role="tabpanel" id="kuwait">
						<div class="row">
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Maysam Al Duaij.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Maysam AlDuaij</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Technical Business Analysis Expert</span>
									<a href="#" class="abs-link meetMentorsCard__link" data-path="src/data/kuwait/Maysam-AlDuaij.html" data-toggle="custom-modal" data-size="large"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Dana Abdal.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">DANA ABDAL</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Care Applications Management Specialist</span>
									<a href="#" class="abs-link meetMentorsCard__link" data-path="src/data/kuwait/Dana-abdal.html" data-toggle="custom-modal" data-size="large"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Nadia Al Saif.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Nadia AlSaif</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Business Intelligence Director</span>
									<a href="#" class="abs-link meetMentorsCard__link" data-path="src/data/kuwait/Nadia-al-saif.html" data-toggle="custom-modal" data-size="large"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Mohammed Al-Murshed Zain Group.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Mohammad Al Murshed</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Networks Director</span>
									<a href="#" class="abs-link meetMentorsCard__link" data-path="src/data/kuwait/M.Al-murshad.html" data-toggle="custom-modal" data-size="large"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Mohd Tarawneh Zain Group.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Mohammad Al Tarawneh</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Network Transmission Manager</span>
									<a href="#" class="abs-link meetMentorsCard__link" data-path="src/data/kuwait/M.Al-tarawneh.html" data-toggle="custom-modal" data-size="large"></a>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" role="tabpanel" id="jordan">
						<div class="row">
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Hiba Afaneh Zain Jordan.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Hiba Nader Awad Afaneh</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Enterprise Risk Management and Internal Audit</span>
									<a href="#" data-path="src/data/jordan/Hiba-Nader-Awad-Afaneh.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Dalia Shakhshir Zain Jordan.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Dalia Mohammed Ramez Shakhshir</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Consumer Business</span>
									<a href="#" data-path="src/data/jordan/Dalia-Mohammed-Ramez-Shakhshir.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Lama Rahwanji Zain Jordan.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Lama (Mohammad Said) Musallam Al Rahawanjy</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Legal & Regulatory Affairs</span>
									<a href="#" data-path="src/data/jordan/Lama-(Mohammad Said)-Musallam-Al-Rahawanjy.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Alaa-Abdelqader.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Alaa-Abdelqader</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination"></span>
									<a href="#" data-path="src/data/jordan/Alaa-Abdelqader.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/yahya-Najjar.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">yahya-Najjar</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination"></span>
									<a href="#" data-path="src/data/jordan/yahya-Najjar.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Raad Qandah.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Raad Qandah</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Network Performance Analyst</span>
									<a href="#" data-path="src/data/jordan/Raad-Qandah.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" role="tabpanel" id="ksa">
						<div class="row">
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/unnamed.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Ghadah Mutaab AlMutairi</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Workforce Management & Planning Manager</span>
									<a href="#" data-path="src/data/ksa/Ghadah-Mutaab-AlMutairi.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Ali Al Salam Zain KSA.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Ali Saleh Ahmad Al Salam</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Invoice Acceptance & Fixed Assets Senior Manager</span>
									<a href="#" data-path="src/data/ksa/Ali-Saleh-Ahmad-Al-Salam.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Bouthina Mahroos Zain KSA.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Bouthina Ahmed Mahroos</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Acting Payroll Senior Manager</span>
									<a href="#" data-path="src/data/ksa/Bouthina-Ahmed-Mahroos.html" data-toggle="custom-modal" data-size="large"  class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Rayan Jan Zain KSA.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Rayan Fawaz Jan</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">ERP Applications Senior Manager</span>
									<a href="#" data-path="src/data/ksa/Rayan-Fawaz-Jan.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Maryam Abdullatif Alhabab Zain KSA.jpeg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Maryam Abdullatif Alhabab</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Budgeting & Control Manager</span>
									<a href="#" data-path="src/data/ksa/Maryam-Abdullatif-Alhabab.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" role="tabpanel" id="iraq">
						<div class="row">
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Haider-Aied-Ali-Alkarway-Zain-Iraq.png" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Haider Aied Ali Alkarway</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Head of Corporate Reporting Unit</span>
									<a href="#" data-path="src/data/iraq/Haider-Aied-Ali-Alkarway.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Rasha Uday Zain Iraq.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Rasha Uday Moain Moain</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Head of Customer Experience Department</span>
									<a href="#" data-path="src/data/iraq/Rasha-Uday-Moain-Moain.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Karim Rahimah Hussein Zain Iraq.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Karim Rahimah Hussein</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Head of Deployment Department</span>
									<a href="#" data-path="src/data/iraq/Karim-Rahimah-Hussein.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Suhaib Wamidh Dhyaa Eldeen Al Naftaji  Zain Iraq.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Suhaib Wamidh Dhyaa Eldeen Al Naftaji</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Head of Core Network Implementation Unit</span>
									<a href="#" data-path="src/data/iraq/Suhaib-Wamidh-Dhyaa-Eldeen-Al-Naftaji.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Ammar-Mohamed-Taher-Al-Shaikly-Zain-Iraq.png" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Ammar Mohamed Taher Al-Shaikly</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Head of Core Planning & Optimization Dept</span>
									<a href="#" data-path="src/data/iraq/Ammar-Mohamed-Taher-Al-Shaikly.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" role="tabpanel" id="bahrain">
						<div class="row">
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Abdulla AlBinAli.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Abdulla Mohammed Albinali</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Radio Specialist</span>
									<a href="#" data-path="src/data/bahrain/Abdulla-Mohammed-Albinali.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/ali atiah .jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Ali Atiah</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Manager, Infrastructure, Core & VAS</span>
									<a href="#" data-path="src/data/bahrain/Ali-Atiah.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Mubarak Nass.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Mubarak Ebrahim Nass</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Infrastructure & Core Specialist </span>
									<a href="#" data-path="src/data/bahrain/Mubarak-Ebrahim-Nass.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" role="tabpanel" id="sudan">
						<div class="row">
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Abueissa.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Mohammed Khaled Omran</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Marketing Segments - Directors</span>
									<a href="#" data-path="src/data/sudan/Mohammed-Khaled-Omran.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Abdelnasir Moutasim.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Abdelnasir Moutasim Elbashir Ali </h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Technology IT Development Director</span>
									<a href="#" data-path="src/data/sudan/Abdelnasir-Moutasim-Elbashir-Ali.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Ghaidaa.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Ghaidaa Abdelgadir Sirelkhatim </h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">strategy and business planning</span>
									<a href="#" data-path="src/data/sudan/Ghaidaa-Abdelgadir-Sirelkhatim.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/omer-kamal.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Omer Kamal Ali Mukhtar</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Telecom Management professional</span>
									<a href="#" data-path="src/data/sudan/Omer-Kamal Ali-Mukhtar.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Nasreen’s.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Nasreen Ibraheem Ali</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Head of Wholesale</span>
									<a href="#" data-path="src/data/sudan/Nasreen-Ibraheem-Ali.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" role="tabpanel" id="southSudan">
						<div class="row">
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Chikijwok.jpeg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Chikijwok Lwal John</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Roaming and Interconnect Specialist</span>
									<a href="#" data-path="src/data/south-sudan/Chikijwok-Lwal-John.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Daniel.jpeg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Daniel Loro </h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Planning and Implementation Team Leader</span>
									<a href="#" data-path="src/data/south-sudan/Daniel-Loro.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Eliza.jpg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Eliza Isaiah </h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Branding and Communication Manager</span>
									<a href="#" data-path="src/data/south-sudan/Eliza-Isaiah.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Gaudensio.jpeg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Gaudensio Modi</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">Radio and Transmission Operation Team Leader</span>
									<a href="#" data-path="src/data/south-sudan/Gaudensio-Modi.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
							<div class="col-lg-3 col-4 mb-30 meetMentorsMain__detail--item">
								<div class="meetMentorsCard">
									<div class="meetMentorsCard__image">
										<img src="src/images/meetMentors/Roynina.jpeg" alt="" class="img-fluid"/>
									</div>
									<h4 class="meetMentorsCard--title">Roynina Magaya</h4>
									<span v-if="directorInfo.jobTitle" class="meetMentorsCard--destination">BSS Application Manager</span>
									<a href="#" data-path="src/data/south-sudan/Roynina-Magaya.html" data-toggle="custom-modal" data-size="large" class="abs-link meetMentorsCard__link"></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include_once "includes/footer.php";?>