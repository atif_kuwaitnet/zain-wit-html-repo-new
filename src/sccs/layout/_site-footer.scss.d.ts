import { StyleProp, TextStyle } from 'react-native'

export const footer-wrapper: StyleProp<TextStyle>
export const logo: StyleProp<TextStyle>
export const main-links: StyleProp<TextStyle>
export const social-links: StyleProp<TextStyle>
export const fb: StyleProp<TextStyle>
export const left-bg-wrapper: StyleProp<TextStyle>
export const flex-column-wrapper: StyleProp<TextStyle>
export const category: StyleProp<TextStyle>
export const copyright: StyleProp<TextStyle>
export const nav-pills: StyleProp<TextStyle>
export const button-row: StyleProp<TextStyle>
export const footer-categories: StyleProp<TextStyle>
