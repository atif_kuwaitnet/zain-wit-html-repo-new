<!DOCTYPE html>
<html lang="en" >  
<head>
    <meta charset="UTF-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <title>Zain Wit</title>
	<link rel="icon" href="src/images/favicon.ico" type="image/gif" sizes="16x16">
    <!-- bootstrap css  -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet">
    <!-- font-awsome css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <!-- Swiper -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/7.0.1/swiper-bundle.css" integrity="sha512-Fp08V+RWsgLTrE/mydkeVu2Riu+nG0I1zahQHo/7On0kiYWakR0B5ErE9nqFI08YzZTSNoEL3nDrXjVQNyNHxw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- custom SCROLL -->
    <link href="src/css/jquery.mCustomScrollbar.css" rel="stylesheet">
    <!-- bootstrap select -->
    <link href="src/css/bootstrap-select.css" rel="stylesheet">
    <!-- main css -->
    <link href="src/css/main.css" type="text/css" rel="stylesheet">
</head>

<body 
<?php if(isset($ishome)){ echo "class='front-page'";}?> 
>
    <div class="modal custom-modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog ">
            <div class="modal-content">
                <button type="button" class="popup-cross-icon" data-dismiss="modal" aria-hidden="true">
                </button>
                <div class="modal-body">
                    
                </div>
            </div>
        </div>
    </div>

    <div id="loader-wrapper">
        <div id="loader">
            <img src="src/images/WIT_Banner1.jpg" alt="" />
        </div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div> 
    </div>    
    
    <?php
    include_once 'includes/menu.php';  
    ?>
   
    <main role="main">
    <section class="section-1">
		<header role="banner" >
			<a class="skip-main" href="#main">Skip to main content</a>
			<a class="skip-main" href="#footer">Skip to Footer</a>
			<div class="container">
				<div class="row justify-content-between align-items-center">
					<div class="col-8 col-sm-6 left-side">
					<ul class="list-inline align-items-center" role="presentation">
						<li>
						<a
							href="#"
							role="button"
							aria-label="Main Navigation"
							class="menu" id="menu"
						>
							
							<svg fill="currentColor" viewBox="0 0 512 459.49">
								<g id="menuIcon">
									<path
										class="cls-1"
										d="M479.18,65.64H32.82A32.82,32.82,0,0,1,32.82,0H479.18a32.82,32.82,0,0,1,0,65.64Z"
									/>
									<path
										class="cls-1"
										d="M295.38,262.56H32.82a32.82,32.82,0,0,1,0-65.64H295.38a32.82,32.82,0,1,1,0,65.64Z"
									/>
									<path
										class="cls-2"
										d="M479.18,262.56H426.67a32.82,32.82,0,1,1,0-65.64h52.51a32.82,32.82,0,0,1,0,65.64Z"
									/>
									<path
										class="cls-1"
										d="M479.18,459.49H32.82a32.82,32.82,0,0,1,0-65.64H479.18a32.82,32.82,0,0,1,0,65.64Z"
									/>
								</g>
							</svg>
						</a>
						</li>
						<li>
						<a href="#" title="Zain">
							<img
							src="src/images/svg/logo.svg"
							class="logo img-fluid"
							alt="zain logo"
							/>
						</a>
						</li>
					</ul>
					</div>
					<div class="col-4 col-sm-6 right-side">
					<ul class="list-inline" role="presentation">
						<li class="langSelector">
						<a href="#" lang="ar">
							<span>العربية</span>
							<div class="langIcon">
							<img
								src="src/images/icons/arabic-icon.png"
								alt=""
							/>
							</div>
						</a>
						</li>
					</ul>
					</div>
				</div>
			</div>
		</header>
	</section> 
        


        
     
        

